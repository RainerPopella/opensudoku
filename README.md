# Open Sudoku

[Open Sudoku](https://opensudoku.moire.org/) is an open-source Sudoku game. It continuously improved
by community to be up to date with the current Android coding standards and supplied with new
functionalities and improvements for the already existing ones.

It's designed to be controlled both by finger and keyboard.
It's preloaded with 90 puzzles in 3 difficulty levels.
More puzzles can be downloaded from the web, and it also allows you to enter your own puzzles.

## Download

[<img src="https://fdroid.gitlab.io/artwork/badge/get-it-on.png"
alt="Get it on F-Droid"
height="80">](https://f-droid.org/packages/org.moire.opensudoku/)
[<img src="https://opensudoku.moire.org/img/get-it-on-gitlab.png"
alt="Get it on GitLab"
height="80">](https://gitlab.com/opensudoku/opensudoku/-/releases)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en_badge_web_generic.png"
alt="Get it on Google Play"
height="80">](https://play.google.com/store/apps/details?id=org.moire.opensudoku)

## Testing incoming versions through Play Store

You can test incoming versions of Open Sudoku before they get released at Google Play Store.
To join testers go to https://play.google.com/apps/testing/org.moire.opensudoku

## Authors and Contributors

Open Sudoku since version 4 is authored by [Bart Uliasz](https://github.com/buliasz).

Previous versions of Open Sudoku were authored by [Óscar García Amor](https://ogarcia.me/).

The first version of Open Sudoku was developed by [Roman Mašek](https://github.com/romario333) and
was contributed to by Vit Hnilica, Martin Sobola, Martin Helff, and Diego Pierotto.

### Main Contributors

* [Rainer Popella](https://gitlab.com/RainerPopella)
* [Nik Clayton](https://gitlab.com/nikclayton)
* [Sergey Pimanov](https://github.com/spimanov)
* [TacoTheDank](https://github.com/TacoTheDank)
* [steve3424](https://github.com/steve3424)
* [Justin Nordin](https://github.com/jlnordin)
* [Chris Lane](https://github.com/ChrisLane)
* [Jesus Fuentes](https://github.com/fuentesj11)
* [Daniel](https://github.com/demield)
* [Роман](https://github.com/D0ct0rZl0)
