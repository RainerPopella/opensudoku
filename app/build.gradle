/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2025-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//file:noinspection HardCodedStringLiteral
apply plugin: 'com.android.application'
apply plugin: 'org.jetbrains.kotlin.android'

android {
	defaultConfig {
		applicationId = 'org.moire.opensudoku'
		minSdk = 26
		targetSdk = 34
		versionCode = 20250301
		versionName = '4.4.0'

		vectorDrawables.useSupportLibrary = true
	}

	buildTypes {
		release {
			// This is the release version
			minifyEnabled = true
			shrinkResources = true
			proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
			ndk.debugSymbolLevel = 'FULL'
		}
		debug {
			// This is a debug version
			// - it will not override a release version
			// - it can be installed in parallel to a release version on a device
			// - it use the release applicationId, but with an suffix '-DEBUG'
			minifyEnabled = false
			shrinkResources = false
			proguardFiles getDefaultProguardFile('proguard-android-optimize.txt'), 'proguard-rules.pro'
			versionNameSuffix = '-DBG'
			applicationIdSuffix = '.debug'
		}
		demo {
			// This is a demo version, can be also used for BETA or RC or TEST versions
			// - it will not override a release version
			// - it can be installed in parallel to a release version on a device
			// - it use the release applicationId, but with an suffix '-DEMO'
			minifyEnabled = true
			shrinkResources = true
			proguardFiles getDefaultProguardFile('proguard-android.txt'), 'proguard-rules.pro'
			versionNameSuffix = '-DMO'
			applicationIdSuffix = '.demo'
			signingConfig = signingConfigs.debug
		}
	}


	compileOptions {
		encoding = 'UTF-8'
		sourceCompatibility = JavaVersion.VERSION_17
		targetCompatibility = JavaVersion.VERSION_17
	}
	kotlinOptions {
		jvmTarget = '17'
	}
	packagingOptions {
		jniLibs {
			excludes += ['META-INF/*']
		}
		resources {
			excludes += ['META-INF/*']
		}
	}

	namespace = 'org.moire.opensudoku'
	lint {
		checkReleaseBuilds = false
	}
	buildFeatures {
		buildConfig = true
	}
	compileSdk = 34
}

dependencies {
	implementation 'androidx.preference:preference:1.2.1'
	implementation 'com.google.android.material:material:1.12.0'

	// The original net.margaritov.preference.colorpicker.ColorPickerPreference does not work
	// with androidx.preference.Preference.
	// This fork does, but is not published to a repository, so use the module direct from
	// GitHub. Gradle source dependencies do not support specifying a git ref or tag, so for
	// reproducibility, use the tag and depend on jitpack.io in the parent build.gradle.
	implementation 'com.github.attenzione:android-ColorPickerPreference:1.2.1'

	// dependencies needed for Sudoku scanning
	implementation 'org.opencv:opencv:4.11.0' // camera view
	implementation 'com.google.android.gms:play-services-mlkit-text-recognition:19.0.1' // text recognition
}
