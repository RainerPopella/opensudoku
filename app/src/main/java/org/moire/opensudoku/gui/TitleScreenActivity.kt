/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2023 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.gui

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Button
import android.widget.Toast
import androidx.activity.viewModels
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import org.moire.opensudoku.R
import org.moire.opensudoku.db.SudokuDatabase
import org.moire.opensudoku.game.GameSettings
import org.moire.opensudoku.game.SudokuBoard
import org.moire.opensudoku.game.SudokuGame
import org.moire.opensudoku.game.SudokuGame.Companion.GAME_STATE_NOT_STARTED
import org.moire.opensudoku.gui.Tag.PUZZLE_ID
import org.moire.opensudoku.gui.fragments.AboutDialogFragment
import org.moire.opensudoku.utils.OptionsMenuItem
import org.moire.opensudoku.utils.addAll
import java.time.Instant
import java.time.LocalDate
import kotlin.getValue

private var firstOnCreate: Boolean = true

class TitleScreenActivity : ThemedActivity() {
	val viewModel: DbKeeperViewModel by viewModels()
	private lateinit var resumeButton: Button
	private lateinit var randomUnsolvedButton: Button
	private lateinit var challengeButton: Button
	private lateinit var aboutDialog: AboutDialogFragment
	private var isInitialized = false

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_title_screen)
		resumeButton = findViewById(R.id.resume_button)
		randomUnsolvedButton = findViewById(R.id.random_unsolved_button)
		challengeButton = findViewById(R.id.challenge_button)
		val puzzleListButton = findViewById<Button>(R.id.puzzle_lists_button)
		val settingsButton = findViewById<Button>(R.id.settings_button)
		puzzleListButton.setOnClickListener { startActivity(Intent(this, FolderListActivity::class.java)) }
		settingsButton.setOnClickListener { startActivity(Intent(this, GameSettingsActivity::class.java)) }
		aboutDialog = AboutDialogFragment()

		// check the preference to skip the title screen and launch the folder list activity directly
		if (firstOnCreate) {
			val gameSettings = GameSettings(applicationContext)
			val showPuzzleFolderListOnStartup = gameSettings.isShowPuzzleListOnStartup
			if (showPuzzleFolderListOnStartup) {
				startActivity(Intent(this, FolderListActivity::class.java))
			} else { // show changelog on first run
				Changelog.showOnFirstRun(this)
			}
			firstOnCreate = false
		}

		viewModel.initDb(this, true) { db ->
			isInitialized = true
			updateView()
		}
	}

	private fun canResume(db: SudokuDatabase, sudokuGameID: Long): Boolean {
		val sudokuGame = db.getPuzzle(sudokuGameID, false) ?: return false
		return sudokuGame.state != SudokuGame.GAME_STATE_COMPLETED
	}

	private fun ensureDecemberChallengePuzzle(db: SudokuDatabase): Long {
		val xmasPuzzleValues = "690040013400792008000010000000409000004573200000020000000108000007000300041237890"
		val xmasPuzzle = db.findPuzzle(xmasPuzzleValues, false)
		if (xmasPuzzle != null) {
			return xmasPuzzle.id
		}

		val challengeFolderInfo = db.insertFolder(getString(R.string.challenge), 0)
		val (newBoard, _) = SudokuBoard.deserialize(xmasPuzzleValues, false)
		SudokuGame(newBoard).run {
			state = GAME_STATE_NOT_STARTED
			created = Instant.now().toEpochMilli()
			userNote = getString(R.string.xmas_challenge)
			folderId = challengeFolderInfo.id
			return@ensureDecemberChallengePuzzle db.insertPuzzle(this)
		}
	}

	// shows either resume button or random unsolved puzzle button. but never both at a time.
	private fun setupResumeAndRandomButton(db: SudokuDatabase) {
		val resumePuzzleID = GameSettings(applicationContext).recentlyPlayedPuzzleId
		if (canResume(db, resumePuzzleID)) {
			resumeButton.visibility = View.VISIBLE
			resumeButton.setOnClickListener {
				val intentToPlay = Intent(this@TitleScreenActivity, SudokuPlayActivity::class.java)
				intentToPlay.putExtra(PUZZLE_ID, resumePuzzleID)
				startActivity(intentToPlay)
			}
			randomUnsolvedButton.visibility = View.GONE
		} else {
			resumeButton.visibility = View.GONE

			val randomPuzzleID = db.getRandomPuzzleID(true)
			if (randomPuzzleID == null) {
				randomUnsolvedButton.visibility = View.GONE
				return
			}
			randomUnsolvedButton.visibility = View.VISIBLE
			randomUnsolvedButton.setOnClickListener {
				val intentToPlay = Intent(this@TitleScreenActivity, SudokuPlayActivity::class.java)
				intentToPlay.putExtra(PUZZLE_ID, randomPuzzleID)
				startActivity(intentToPlay)
			}
		}
	}

	private fun setupChallengeButton(db: SudokuDatabase) {
		if (LocalDate.now().month.value == 12) {
			val puzzleID = ensureDecemberChallengePuzzle(db)
			challengeButton.text = getString(R.string.xmas_challenge)
			challengeButton.visibility = View.VISIBLE
			challengeButton.setOnClickListener {
				val intentToPlay = Intent(this@TitleScreenActivity, SudokuPlayActivity::class.java)
				intentToPlay.putExtra(PUZZLE_ID, puzzleID)
				startActivity(intentToPlay)
			}
		} else {
			challengeButton.visibility = View.GONE
		}
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		super.onCreateOptionsMenu(menu)
		if (!isInitialized) return false

		menu.addAll(OptionsMenuItems.entries)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			OptionsMenuItems.SETTINGS.id -> {
				startActivity(Intent(this, GameSettingsActivity::class.java))
				return true
			}

			OptionsMenuItems.ABOUT.id -> {
				aboutDialog.show(supportFragmentManager, "AboutDialog")
				return true
			}
		}
		return super.onOptionsItemSelected(item)
	}

	override fun onResume() {
		super.onResume()
		if (isInitialized) updateView()
	}

	fun updateView() {
		viewModel.db!!.let { db -> // can be that the buttons showing changed in the meantime
			setupResumeAndRandomButton(db)
			setupChallengeButton(db)
		}
	}

	companion object {
		enum class OptionsMenuItems(
			@StringRes override val titleRes: Int, @DrawableRes override val iconRes: Int, override val shortcutKey: Char
		) : OptionsMenuItem {
			SETTINGS(R.string.settings, R.drawable.ic_settings, 's'),
			ABOUT(R.string.about, R.drawable.ic_info, 'h');

			override val id = ordinal + Menu.FIRST
			override val isAction: Boolean = false
		}
	}
}
