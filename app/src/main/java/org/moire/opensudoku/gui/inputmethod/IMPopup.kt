/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.moire.opensudoku.gui.inputmethod

import android.content.Context
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.core.view.isVisible
import com.google.android.material.button.MaterialButton
import org.moire.opensudoku.R
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.CellMarks
import org.moire.opensudoku.game.SudokuBoard

class IMPopup(val parent: ViewGroup) : InputMethod() {

	/**
	 * If set to true, buttons for numbers, which occur in [SudokuBoard]
	 * more than [SudokuBoard.SUDOKU_SIZE]-times, will be highlighted.
	 */
	private var editCellDialog: IMPopupDialog? = null
	private lateinit var selectedCell: Cell
	override var switchModeButton: Button? = null

	override var centralMarksButton: MaterialButton? = null
		get() = editCellDialog?.centralMarksButton

	override var cornerMarksButton: MaterialButton? = null
		get() = editCellDialog?.cornerMarksButton

	/**
	 * Occurs when user selects number in EditCellDialog.
	 * Occurs when user edits marks in EditCellDialog
	 */
	private fun onCellUpdate(value: Int, centralMarks: Array<Int>, cornerMarks: Array<Int>) {
		var manualRecorded = game.setCellCentralMarks(selectedCell, CellMarks.fromIntArray(centralMarks), true)
		manualRecorded = game.setCellCornerMarks(selectedCell, CellMarks.fromIntArray(cornerMarks), !manualRecorded) || manualRecorded
		if (value != -1) {
			val newValue = if (value == selectedCell.value) 0 else value
			game.setCellValue(selectedCell, newValue, !manualRecorded)
			boardView.highlightedValue = newValue
		}
	}

	/**
	 * Occurs when popup dialog is closed.
	 */
	private val onPopupDismissedListener = DialogInterface.OnDismissListener { _: DialogInterface? -> boardView.hideTouchedCellHint() }

	private fun ensureEditCellDialog() {
		if (editCellDialog == null) {
			editCellDialog = IMPopupDialog(parent, context, boardView, controlPanel).apply {
				cellUpdateCallback = ::onCellUpdate
				setOnDismissListener(onPopupDismissedListener)
				setShowNumberTotals(controlPanel.showDigitCount)
				setHighlightCompletedValues(controlPanel.highlightCompletedValues)
				digitButtons = numberButtons
				cornerMarksButton.isVisible = controlPanel.isDoubleMarksEnabled
			}
		}
	}

	override fun onActivated() {
		boardView.autoHideTouchedCellHint = false
	}

	override fun onDeactivated() {
		boardView.autoHideTouchedCellHint = true
	}

	override fun update() {
	}

	override fun onCellTapped(cell: Cell) {
		selectedCell = cell
		if (cell.isEditable || boardView.board.isEditMode) {
			ensureEditCellDialog()
			editCellDialog!!.resetState()
			editCellDialog!!.setNumber(cell.value)
			editCellDialog!!.setCentralMarks(cell.centralMarks.marksValues)
			editCellDialog!!.setCornerMarks(cell.cornerMarks.marksValues)
			val valuesUseCount = game.board.valuesUseCount
			editCellDialog!!.setValueCount(valuesUseCount)
			editCellDialog!!.show()
		} else {
			boardView.hideTouchedCellHint()
		}
	}

	override fun onCellSelected(cell: Cell?) {
		super.onCellSelected(cell)
		boardView.highlightedValue = cell?.value ?: 0
	}

	override fun onPause() {
		// release dialog resource (otherwise WindowLeaked exception is logged)
		editCellDialog?.cancel()
	}

	override val nameResID: Int
		get() = R.string.popup
	override val helpResID: Int
		get() = R.string.im_popup_hint
	override val abbrName: String
		get() = context.getString(R.string.popup_abbr)

	override fun createControlPanelView(abbrName: String): View {
		val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
		val imPanelView = inflater.inflate(R.layout.im_popup, parent, false)

		switchModeButton = imPanelView.findViewById<Button>(R.id.popup_switch_input_mode).apply {
			text = abbrName
			isEnabled = controlPanel.isSwitchModeButtonEnabled
			setTextColor(makeTextColorStateList(boardView))
			backgroundTintList = makeBackgroundColorStateList(boardView)
		}

		return imPanelView
	}
}
