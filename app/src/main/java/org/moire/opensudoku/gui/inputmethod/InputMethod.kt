/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.moire.opensudoku.gui.inputmethod

import android.content.Context
import android.content.res.ColorStateList
import android.view.View
import android.widget.Button
import androidx.annotation.ColorInt
import com.google.android.material.button.MaterialButton
import org.moire.opensudoku.R
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.SudokuGame
import org.moire.opensudoku.gui.NumberButton
import org.moire.opensudoku.gui.SudokuBoardView
import kotlin.math.ceil

const val COLOR_DIM_FACTOR = 0.33

/**
 * Base class for several input methods used to edit puzzle contents.
 */
abstract class InputMethod {
	abstract var switchModeButton: Button?
	abstract var centralMarksButton: MaterialButton?
	abstract var cornerMarksButton: MaterialButton?

	protected lateinit var context: Context
	protected lateinit var controlPanel: IMControlPanel
	protected lateinit var game: SudokuGame
	protected lateinit var boardView: SudokuBoardView
	protected var isActive = false
	var inputMethodView: View? = null
	internal var digitButtons: MutableMap<Int, NumberButton>? = null

	/**
	 * This should be unique name of input method.
	 */
	var inputMethodName: String? = null
		private set

	open fun initialize(context: Context, controlPanel: IMControlPanel, game: SudokuGame, board: SudokuBoardView) {
		this.context = context
		this.controlPanel = controlPanel
		this.game = game
		boardView = board
		inputMethodName = this.javaClass.simpleName
	}

	fun createInputMethodView(): View {
		val inputMethodView = createControlPanelView(abbrName)
		this.inputMethodView = inputMethodView
		return inputMethodView
	}

	/**
	 * This should be called when activity is paused (InputMethod can do some cleanup for example properly dismiss dialogs preventing WindowLeaked exception).
	 */
	open fun onPause() {}

	abstract val nameResID: Int
	abstract val helpResID: Int

	/**
	 * Gets abbreviated name of input method, which will be displayed on input method switch button.
	 */
	abstract val abbrName: String
	var isEnabled: Boolean = false
		set(enabled) {
			field = enabled
			if (!enabled) {
				controlPanel.activateNextInputMethod()
			}
			controlPanel.onImEnabledChange()
		}

	fun activate() {
		isActive = true
		onActivated()
	}

	fun deactivate() {
		isActive = false
		onDeactivated()
	}

	protected abstract fun createControlPanelView(abbrName: String): View
	protected open fun onActivated() {}
	protected open fun onDeactivated() {}
	abstract fun update()

	/**
	 * Called when cell is selected. Please note that cell selection can change without direct user interaction.
	 */
	open fun onCellSelected(cell: Cell?) {}

	/**
	 * Called when cell is tapped.
	 */
	open fun onCellTapped(cell: Cell) {}

	companion object {
		const val MODE_EDIT_VALUE = 0
		const val MODE_EDIT_CENTRAL_MARKS = 1
		const val MODE_EDIT_CORNER_MARKS = 2

		/**
		 * Generates a [ColorStateList] using colors from boardView suitable
		 * for use as text colors on a button.
		 *
		 *
		 * An XML color state list file can not be used because the colors may be
		 * changed at runtime instead of coming from a fixed theme.
		 *
		 * @param boardView the view to derive colors from
		 * @return suitable colors
		 * @see .makeBackgroundColorStateList
		 */
		// Note: It's tempting to make this part of NumberButton, but it's useful for other buttons
		// that are not NumberButton (e.g., the delete and mode switch buttons).
		fun makeTextColorStateList(boardView: SudokuBoardView): ColorStateList {
			val states = arrayOf(
				intArrayOf(R.attr.all_numbers_placed, android.R.attr.state_enabled),
				intArrayOf(android.R.attr.state_enabled, android.R.attr.state_checked),
				intArrayOf(android.R.attr.state_enabled),
				intArrayOf()
			)

			// The number being entered, or highlighted, so use the same colour as highlighted digits
			val allNumbersPlacedText = setAlpha(boardView.textReadOnly.color, COLOR_DIM_FACTOR)
			val selectedText = boardView.textHighlighted.color
			val notSelectedText = boardView.value.color
			val disabledText: Int = setAlpha(boardView.textReadOnly.color, COLOR_DIM_FACTOR)
			val colors = intArrayOf(
				allNumbersPlacedText, selectedText, notSelectedText, disabledText
			)
			return ColorStateList(states, colors)
		}

		/**
		 * Generates a [ColorStateList] using colors from boardView suitable
		 * for use as background colors on a button.
		 *
		 *
		 * An XML color state list file can not be used because the colors may be
		 * changed at runtime instead of coming from a fixed theme.
		 *
		 * @param boardView the view to derive colors from
		 * @return suitable colors
		 * @see .makeTextColorStateList
		 */
		fun makeBackgroundColorStateList(boardView: SudokuBoardView): ColorStateList {
			val states = arrayOf(
				intArrayOf(R.attr.all_numbers_placed, android.R.attr.state_enabled, -android.R.attr.state_checked),
				intArrayOf(android.R.attr.state_enabled, android.R.attr.state_checked),
				intArrayOf(android.R.attr.state_enabled),
				intArrayOf()
			)

			// The number being entered, or highlighted, so use the same colour as highlighted digits
			val allNumbersPlacedBackground = boardView.backgroundReadOnly.color
			val selectedBackground = boardView.backgroundHighlighted.color
			val notSelectedBackground = boardView.backgroundReadOnly.color
			val disabledBackground = boardView.background.color
			val colors = intArrayOf(allNumbersPlacedBackground, selectedBackground, notSelectedBackground, disabledBackground)
			return ColorStateList(states, colors)
		}

		/**
		 * Adjusts the alpha channel of the given color.
		 *
		 * @param color color to adjust
		 * @param alphaPct percentage to adjust it by
		 * @return The adjusted color
		 */
		@ColorInt
		private fun setAlpha(@ColorInt color: Int, @Suppress("SameParameterValue") alphaPct: Double): Int {
			val colorOnly = color and 0x00ffffff
			val alphaShifted = ceil(alphaPct * 0xFF).toInt() shl 24
			return colorOnly or alphaShifted
		}
	}
}
