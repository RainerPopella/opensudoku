/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.gui

import android.R.attr.colorError
import android.annotation.SuppressLint
import android.content.Context
import android.database.Cursor
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import org.moire.opensudoku.R
import org.moire.opensudoku.db.extractSudokuGameFromCursorRow
import org.moire.opensudoku.game.SudokuGame
import org.moire.opensudoku.utils.ThemeUtils
import org.moire.opensudoku.utils.addAll
import java.io.Closeable
import java.time.Instant
import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

internal class PuzzleListRecyclerAdapter(
	private val context: Context,
	private var puzzlesCursor: Cursor,
	private val onClickListener: (Long) -> Unit,
	private var isShowTime: Boolean,
	private var isShowMistakesCount: Boolean
) : RecyclerView.Adapter<PuzzleListRecyclerAdapter.ViewHolder?>(), Closeable {
	private val localZoneOffset: ZoneOffset = ZoneId.systemDefault().rules.getOffset(Instant.now())
	var selectedGameId: Long = 0
	private val playingDurationFormatter = PlayingDurationFormat()
	private val dateTimeFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
	private val timeFormatter = DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT)

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
		val view = LayoutInflater.from(parent.context).inflate(R.layout.sudoku_list_item, parent, false)
		return ViewHolder(view)
	}

	override fun getItemCount(): Int = puzzlesCursor.count

	@SuppressLint("NotifyDataSetChanged")
	fun updateGameList(newGames: Cursor, isShowTime: Boolean, isShowMistakesCount: Boolean) {
		this.isShowTime = isShowTime
		this.isShowMistakesCount = isShowMistakesCount
		puzzlesCursor.close()
		puzzlesCursor = newGames
		notifyDataSetChanged()
	}

	@SuppressLint("SetTextI18n")
	override fun onBindViewHolder(holder: ViewHolder, position: Int) {
		holder.itemView.contentDescription = context.getString(R.string.puzzle_selection_position, position)

		//          R.id.board_view
		holder.boardView.isReadOnlyPreview = true
		holder.boardView.isFocusable = false
		ThemeUtils.applyConfiguredThemeToSudokuBoardView(holder.boardView, context)

		CoroutineScope(Dispatchers.IO).launch {
			// data processing
			puzzlesCursor.moveToPosition(position)
			val game: SudokuGame? = extractSudokuGameFromCursorRow(puzzlesCursor, false)
			if (game == null) {
				withContext(Dispatchers.Main) {
					Toast.makeText(context, R.string.error_could_not_find_puzzle_in_database, Toast.LENGTH_LONG).show()
				}
				return@launch
			}

			// precalculate SUID out of UI scope for slow devices
			val suid = if (game.solutionCount == 1) "SUID: " + SuidGenerator3F().encode(game.board) else null

			withContext(Dispatchers.Main) {
				holder.itemView.setOnClickListener { onClickListener(game.id) }
				holder.itemView.setOnCreateContextMenuListener { menu, _, _ ->
					selectedGameId = game.id
					menu?.run { addAll(PuzzleListActivity.ContextMenuItems.entries) }
				}

				holder.boardView.board = game.board

				//          R.id.state
				holder.state.apply {
					when (game.state) {
						SudokuGame.GAME_STATE_COMPLETED -> {
							text = context.getString(R.string.solved)
							setTextColor(ThemeUtils.getCurrentThemeColor(holder.userNote.context, R.attr.colorReadOnlyText))
						}

						SudokuGame.GAME_STATE_PLAYING -> {
							text = context.getString(R.string.playing)
							setTextColor(ThemeUtils.getCurrentThemeColor(holder.userNote.context, R.attr.colorLine))
						}

						else -> {
							text = context.getString(R.string.not_started)
							setTextColor(ThemeUtils.getCurrentThemeColor(holder.userNote.context, R.attr.colorValueText))
						}
					}
				}

				if (game.playingDuration != 0L) {
					val durationText = playingDurationFormatter.format(game.playingDuration)
					holder.mistakesAndDuration.text =
						(if (isShowMistakesCount) "\u2757${game.mistakeCounter}\t" else "") + (if (isShowTime) "\u231A${durationText}" else "")
				}

				holder.lastPlayed.text = if (game.lastPlayed != 0L) context.getString(R.string.last_played_at, getDateAndTimeForHumans(game.lastPlayed)) else ""
				holder.created.text = if (game.created != 0L) context.getString(R.string.created_at, getDateAndTimeForHumans(game.created)) else ""
				holder.userNote.text = if (game.userNote != "") context.getString(R.string.note) + " " + game.userNote else ""
				holder.suid.apply {
					if (suid != null) {
						text = suid
						setTextColor(ThemeUtils.getCurrentThemeColor(holder.userNote.context, R.attr.colorText))
					} else {
						text = context.getString(R.string.invalid_puzzle)
						setTextColor(ThemeUtils.getCurrentThemeColor(holder.userNote.context, colorError))
					}
				}
			}
		}
	}

	private fun getDateAndTimeForHumans(epochMilliseconds: Long): String {
		val dateTime = LocalDateTime.ofEpochSecond(epochMilliseconds / 1000, 0, localZoneOffset)
		val today = LocalDate.now()
		val yesterday = today.minusDays(1)
		return if (dateTime.isAfter(today.atStartOfDay())) {
			context.getString(R.string.today_at_time, dateTime.toLocalTime().format(timeFormatter))
		} else if (dateTime.isAfter(yesterday.atStartOfDay())) {
			context.getString(R.string.yesterday_at_time, dateTime.toLocalTime().format(timeFormatter))
		} else {
			context.getString(R.string.on_date, dateTime.format(dateTimeFormatter))
		}
	}

	internal class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
		val boardView: SudokuBoardView = itemView.findViewById(R.id.board_view)
		val state: TextView = itemView.findViewById(R.id.state)
		val mistakesAndDuration: TextView = itemView.findViewById(R.id.mistakes_and_time)
		val lastPlayed: TextView = itemView.findViewById(R.id.last_played)
		val created: TextView = itemView.findViewById(R.id.created)
		val userNote: TextView = itemView.findViewById(R.id.user_note)
		val suid: TextView = itemView.findViewById(R.id.suid)
	}

	override fun close() {
		puzzlesCursor.close()
	}
}
