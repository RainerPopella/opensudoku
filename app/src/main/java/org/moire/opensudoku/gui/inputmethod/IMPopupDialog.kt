/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.moire.opensudoku.gui.inputmethod

import android.app.Dialog
import android.content.Context
import android.content.res.ColorStateList
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.android.material.button.MaterialButton
import org.moire.opensudoku.R
import org.moire.opensudoku.game.SudokuBoard.Companion.SUDOKU_SIZE
import org.moire.opensudoku.gui.NumberButton
import org.moire.opensudoku.gui.SudokuBoardView

/**
 * Dialog for selecting and entering values and marks.
 *
 * When entering a value, the dialog automatically closes.
 *
 * When entering a mark the dialog remains open, to allow multiple marks to be entered at once.
 */
class IMPopupDialog(val parent: ViewGroup, context: Context, boardView: SudokuBoardView, val controlPanel: IMControlPanel) : Dialog(context) {
	private val inflater: LayoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
	val numberButtons: MutableMap<Int, NumberButton> = HashMap()

	// selected number on "Select number" tab (0 if nothing is selected).
	private var selectedNumber = 0
	private val centralMarksSelectedNumbers: MutableSet<Int> = HashSet()
	private val cornerMarksSelectedNumbers: MutableSet<Int> = HashSet()
	private var showNumberTotals = false

	/** True if buttons with completed values should be highlighted  */
	private var highlightCompletedValues = false
	private val enterNumberButton: MaterialButton
	val centralMarksButton: MaterialButton
	val cornerMarksButton: MaterialButton

	/**
	 * Callback to be invoked when user selects a new cell value and/or central/corner marks.
	 */
	internal lateinit var cellUpdateCallback: ((value: Int, centralMarks: Array<Int>, cornerMarks: Array<Int>) -> Unit)

	private val valueCount: Array<Int> = Array(SUDOKU_SIZE + 1) { 0 }

	private val numberButtonClicked = View.OnClickListener { v: View ->
		val number = v.tag as Int
		when (controlPanel.editMode) {
			InputMethod.MODE_EDIT_VALUE -> {
				selectedNumber = number
				syncAndDismiss()
			}

			InputMethod.MODE_EDIT_CENTRAL_MARKS -> if ((v as MaterialButton).isChecked) {
				centralMarksSelectedNumbers.add(number)
			} else {
				centralMarksSelectedNumbers.remove(number)
			}

			InputMethod.MODE_EDIT_CORNER_MARKS -> if ((v as MaterialButton).isChecked) {
				cornerMarksSelectedNumbers.add(number)
			} else {
				cornerMarksSelectedNumbers.remove(number)
			}
		}
	}

	/**
	 * Occurs when user presses "Clear" button.
	 */
	private val clearButtonListener = View.OnClickListener { v ->
		(v as MaterialButton).isChecked = false
		when (controlPanel.editMode) {
			InputMethod.MODE_EDIT_VALUE -> {
				selectedNumber = 0
				syncAndDismiss()
			}

			InputMethod.MODE_EDIT_CENTRAL_MARKS ->                     // Clear the central marks. Dialog should stay visible
				setCentralMarks(emptyList())

			InputMethod.MODE_EDIT_CORNER_MARKS ->                     // Clear the corner marks. Dialog should stay visible
				setCornerMarks(emptyList())
		}
		update()
	}

	/**
	 * Occurs when user presses "Close" button.
	 */
	private val closeButtonListener = View.OnClickListener { _: View? -> syncAndDismiss() }

	/** Synchronises state with the hosting activity and dismisses the dialog  */
	private fun syncAndDismiss() {
		cellUpdateCallback(selectedNumber, centralMarksSelectedNumbers.toTypedArray(), cornerMarksSelectedNumbers.toTypedArray())
		dismiss()
	}

	init {
		val keypad = inflater.inflate(R.layout.im_popup_edit_value, parent, false)

		numberButtons[1] = keypad.findViewById(R.id.button_1)
		numberButtons[2] = keypad.findViewById(R.id.button_2)
		numberButtons[3] = keypad.findViewById(R.id.button_3)
		numberButtons[4] = keypad.findViewById(R.id.button_4)
		numberButtons[5] = keypad.findViewById(R.id.button_5)
		numberButtons[6] = keypad.findViewById(R.id.button_6)
		numberButtons[7] = keypad.findViewById(R.id.button_7)
		numberButtons[8] = keypad.findViewById(R.id.button_8)
		numberButtons[9] = keypad.findViewById(R.id.button_9)

		val colorValueText: ColorStateList = InputMethod.makeTextColorStateList(boardView)
		val colorBackground: ColorStateList = InputMethod.makeBackgroundColorStateList(boardView)

		for ((key, b) in numberButtons) {
			b.tag = key
			b.setOnClickListener(numberButtonClicked)
			b.enableAllNumbersPlaced = highlightCompletedValues
			b.backgroundTintList = colorBackground
			b.setTextColor(colorValueText)
		}

		val clearButton = keypad.findViewById<MaterialButton>(R.id.button_clear)
		clearButton.tag = 0
		clearButton.setOnClickListener(clearButtonListener)
		clearButton.backgroundTintList = colorBackground
		clearButton.iconTint = colorValueText

		/* Switch mode, and update the UI */
		val modeButtonClicked = View.OnClickListener { v: View ->
			controlPanel.editMode = v.tag as Int
			update()
		}

		enterNumberButton = keypad.findViewById<MaterialButton>(R.id.enter_number).apply {
			tag = InputMethod.MODE_EDIT_VALUE
			setOnClickListener(modeButtonClicked)
			backgroundTintList = colorBackground
			iconTint = colorValueText
		}

		centralMarksButton = keypad.findViewById<MaterialButton>(R.id.central_mark).apply {
			tag = InputMethod.MODE_EDIT_CENTRAL_MARKS
			setOnClickListener(modeButtonClicked)
			backgroundTintList = colorBackground
			iconTint = colorValueText
		}

		cornerMarksButton = keypad.findViewById<MaterialButton>(R.id.corner_mark).apply {
			tag = InputMethod.MODE_EDIT_CORNER_MARKS
			setOnClickListener(modeButtonClicked)
			backgroundTintList = colorBackground
			iconTint = colorValueText
		}

		val closeButton = keypad.findViewById<View>(R.id.button_close)
		closeButton.setOnClickListener(closeButtonListener)
		setContentView(keypad)
	}

	private fun update() {
		// Determine which buttons to check, based on the value / marks in the selected cell
		val buttonsToCheck: List<Int>
		when (controlPanel.editMode) {
			InputMethod.MODE_EDIT_VALUE -> {
				enterNumberButton.isChecked = true
				centralMarksButton.isChecked = false
				cornerMarksButton.isChecked = false
				buttonsToCheck = listOf(selectedNumber)
			}

			InputMethod.MODE_EDIT_CENTRAL_MARKS -> {
				enterNumberButton.isChecked = false
				centralMarksButton.isChecked = true
				cornerMarksButton.isChecked = false
				buttonsToCheck = ArrayList(centralMarksSelectedNumbers)
			}

			InputMethod.MODE_EDIT_CORNER_MARKS -> {
				enterNumberButton.isChecked = false
				centralMarksButton.isChecked = false
				cornerMarksButton.isChecked = true
				buttonsToCheck = ArrayList(cornerMarksSelectedNumbers)
			}

			else ->                 // Can't happen
				buttonsToCheck = ArrayList()
		}
		for (button in numberButtons.values) {
			val tag = button.tag as Int

			// Check the button if necessary
			button.isChecked = buttonsToCheck.contains(tag)

			// Update the count of numbers placed
			if (valueCount.isNotEmpty()) {
				button.setNumbersPlaced(valueCount[tag])
			}
		}
	}

	fun setShowNumberTotals(newShowNumberTotals: Boolean) {
		if (showNumberTotals == newShowNumberTotals) {
			return
		}
		showNumberTotals = newShowNumberTotals
		for (button in numberButtons.values) {
			button.showNumbersPlaced = showNumberTotals
		}
	}

	fun setHighlightCompletedValues(highlightCompletedValues: Boolean) {
		if (this.highlightCompletedValues == highlightCompletedValues) {
			return
		}
		this.highlightCompletedValues = highlightCompletedValues
		for (b in numberButtons.values) {
			b.enableAllNumbersPlaced = this.highlightCompletedValues
		}
	}

	/**
	 * Reset most of the state of the dialog (selected values, marks, etc).
	 *
	 * DO NOT reset the edit mode, for compatibility with the previous code that used a tab.
	 * The selected tab (which was the edit mode) was retained if the dialog was dismissed on
	 * one cell and opened on another.
	 */
	fun resetState() {
		selectedNumber = 0
		centralMarksSelectedNumbers.clear()
		cornerMarksSelectedNumbers.clear()
		valueCount.fill(0)
		update()
	}

	fun setNumber(number: Int) {
		selectedNumber = number
		update()
	}

	fun setCentralMarks(numbers: List<Int>) {
		centralMarksSelectedNumbers.clear()
		centralMarksSelectedNumbers.addAll(numbers)
		update()
	}

	fun setCornerMarks(numbers: List<Int>) {
		cornerMarksSelectedNumbers.clear()
		cornerMarksSelectedNumbers.addAll(numbers)
		update()
	}

	fun setValueCount(count: Array<Int>) {
		valueCount.fill(0)
		count.copyInto(valueCount)
		update()
	}
}
