/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2025-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.gui

import android.content.Context
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.update
import org.moire.opensudoku.game.GameSettings
import org.moire.opensudoku.game.HintHighlight
import org.moire.opensudoku.game.SudokuGame
import org.moire.opensudoku.gui.inputmethod.IMControlPanel

data class SudokuPlayUiState(
	var playedGame: SudokuGame? = null
)

class SudokuPlayViewModel : DbKeeperViewModel() {
	private val _uiState = MutableStateFlow(SudokuPlayUiState())
	val uiState = _uiState.asStateFlow()

	var isInitialized: Boolean = false
		private set
	lateinit var settings: GameSettings

	var game: SudokuGame? = null
		private set
	val lastCommandCell get() = game?.lastCommandCell
	var highlightedCells: Map<HintHighlight, List<Pair<Int, Int>>>? = null

	fun init(context: Context, puzzleID: Long, onInitFinished: ()->Unit) {
		isInitialized = true
		settings = GameSettings(context)
		super.initDb(context, false) {
			db!!.getPuzzle(puzzleID, false)?.let { game ->
				this.game = game
				settings.recentlyPlayedPuzzleId = game.id
				_uiState.update {
					SudokuPlayUiState(
						playedGame = game,
					)
				}
				onInitFinished()
			}
		}
	}

	fun updatePuzzleInDB() {
		game?.let { db?.updatePuzzle(it) }
	}

	override fun onCleared() {
		updatePuzzleInDB()
		super.onCleared()
	}

	fun saveInputState(imControlPanel: IMControlPanel) {
		imControlPanel.saveState(settings)
	}

	fun restoreInputState(imControlPanel: IMControlPanel) {
		imControlPanel.restoreState(settings)
	}
}
