/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.gui.fragments

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.content.res.Configuration
import android.graphics.Color
import android.os.Bundle
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.ColorInt
import androidx.annotation.Keep
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.graphics.ColorUtils
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.lifecycle.Lifecycle
import androidx.preference.ListPreference
import androidx.preference.PreferenceFragmentCompat
import net.margaritov.preference.colorpicker.ColorPickerDialog
import net.margaritov.preference.colorpicker.ColorPickerPreference
import org.moire.opensudoku.R
import org.moire.opensudoku.game.CUSTOM_THEME_UI_MODE
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.GameSettings
import org.moire.opensudoku.gui.SudokuBoardView
import org.moire.opensudoku.utils.CustomTheme
import org.moire.opensudoku.utils.MODE_DARK
import org.moire.opensudoku.utils.MODE_LIGHT
import org.moire.opensudoku.utils.ThemeUtils

/**
 * Preview and set a custom app theme.
 */
@Keep
class CustomThemeFragment : PreferenceFragmentCompat(), OnSharedPreferenceChangeListener, MenuProvider {
	private lateinit var board: SudokuBoardView
	private var copyFromExistingThemeDialog: Dialog? = null
	private var settings: GameSettings? = null

	override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
		setPreferencesFromResource(R.xml.preferences_custom_theme, rootKey)
		settings = GameSettings(requireContext())
		val themeCode = settings!!.theme

		val uiModePref = findPreference<ListPreference>(CUSTOM_THEME_UI_MODE)
		if (themeCode == "custom") {
			uiModePref!!.value = MODE_DARK
		}
		if (themeCode == "custom_light") {
			uiModePref!!.value = MODE_LIGHT
		}
	}

	override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
		// Customise the view to include the game board preview. Do this by inflating the
		// default view and the desired view (as a ViewGroup), and then adding the default view
		// to the view group.
		val defaultView = super.onCreateView(inflater, container, savedInstanceState)
		val viewGroup = inflater.inflate(R.layout.preference_custom_theme, container, false) as ViewGroup
		viewGroup.addView(defaultView)

		// The normal preferences layout (i.e., the layout that defaultView is using) forces the
		// width to match the parent. In landscape mode this shrinks the width of the board to 0.
		//
		// To fix, wait until after initialView has been added to viewGroup (so the layout params
		// are the correct type), then override the width and weight to take up 50% of the screen.
		if (resources.configuration.orientation == Configuration.ORIENTATION_LANDSCAPE) {
			val layoutParams = defaultView.layoutParams as LinearLayout.LayoutParams
			layoutParams.width = 0
			layoutParams.weight = 1f
			defaultView.layoutParams = layoutParams
		}
		requireActivity().setTitle(R.string.screen_custom_theme)
		return viewGroup
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		board = view.findViewById(R.id.board_view)
		prepareGamePreviewView(board)
		val menuHost: MenuHost = requireActivity()
		menuHost.addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
	}

	private fun prepareGamePreviewView(board: SudokuBoardView) {
		board.sameValueHighlightMode = settings!!.sameValueHighlightMode
		board.onCellSelectedListener = { cell: Cell? -> board.highlightedValue = cell?.value ?: 0 }
		ThemeUtils.prepareBoardPreviewView(board)
		updateThemePreview()
	}

	private fun updateThemePreview() {
		val themeName = settings!!.theme
		ThemeUtils.applyThemeToSudokuBoardViewFromContext(themeName, board, requireContext())
	}

	override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
		menuInflater.inflate(R.menu.custom_theme, menu)
	}

	override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
		return when (menuItem.itemId) {
			R.id.copy_from_theme -> {
				showCopyFromExistingThemeDialog()
				true
			}

			R.id.create_from_color -> {
				showCreateFromSingleColorDialog()
				true
			}

			else -> false
		}
	}

	private fun showCopyFromExistingThemeDialog() {
		val builder = AlertDialog.Builder(requireActivity())
		builder.setTitle(R.string.select_theme)
		builder.setNegativeButton(android.R.string.cancel, null)
		val themeNames = requireContext().resources.getStringArray(R.array.theme_names)
		val themeNamesWithoutCustomTheme = themeNames.copyOfRange(0, themeNames.size - 1)
		builder.setItems(themeNamesWithoutCustomTheme) { _: DialogInterface?, which: Int ->
			copyFromExistingThemeIndex(which)
			copyFromExistingThemeDialog?.dismiss()
		}
		val copyFromExistingThemeDialog = builder.create()
		copyFromExistingThemeDialog.setOnDismissListener { this.copyFromExistingThemeDialog = null }
		copyFromExistingThemeDialog.show()
		this.copyFromExistingThemeDialog = copyFromExistingThemeDialog
	}

	private fun showCreateFromSingleColorDialog() {
		val colorDialog = ColorPickerDialog(context, settings!!.colorPrimary, "Choose the base color")
		colorDialog.alphaSliderVisible = false
		colorDialog.hexValueEnabled = true
		colorDialog.setOnColorChangedListener(::createCustomThemeFromSingleColor)
		colorDialog.show()
	}

	private fun copyFromExistingThemeIndex(which: Int) {
		val context = requireContext()
		val themeCode = context.resources.getStringArray(R.array.theme_codes)[which]

		// Copy attributes that correspond to the light or dark context for the theme by preparing
		// a ConfigurationContext that uses the appropriate UI mode.
		val isDarkTheme = ThemeUtils.isDarkTheme(themeCode)
		val configContext: Context
		if (isDarkTheme) {
			val config = context.resources.configuration
			config.uiMode = Configuration.UI_MODE_NIGHT_YES or (config.uiMode and Configuration.UI_MODE_NIGHT_MASK.inv())
			configContext = context.createConfigurationContext(config)
		} else {
			val config = context.resources.configuration
			config.uiMode = Configuration.UI_MODE_NIGHT_NO or (config.uiMode and Configuration.UI_MODE_NIGHT_MASK.inv())
			configContext = context.createConfigurationContext(config)
		}
		val themeWrapper = ContextThemeWrapper(configContext, ThemeUtils.getThemeResourceIdFromString(themeCode))
		GameSettings(context).uiMode = if (isDarkTheme) MODE_DARK else MODE_LIGHT

		// Copy these attributes from a theme...
		val attributes = intArrayOf(
			androidx.appcompat.R.attr.colorPrimary,
			androidx.appcompat.R.attr.colorPrimaryDark,
			R.attr.colorLine,
			R.attr.colorSectorLine,
			R.attr.colorValueText,
			R.attr.colorMarksText,
			R.attr.colorBackground,
			R.attr.colorReadOnlyText,
			R.attr.colorReadOnlyBackground,
			R.attr.colorTouchedText,
			R.attr.colorTouchedMarksText,  // *
			R.attr.colorTouchedBackground,
			R.attr.colorSelectedCellFrame,
			R.attr.colorHighlightedText,
			R.attr.colorHighlightedMarksText,  // *
			R.attr.colorHighlightedBackground,
			R.attr.colorInvalidText,
			R.attr.colorInvalidBackground,
			R.attr.colorEvenText,
			R.attr.colorEvenMarksText,
			R.attr.colorEvenBackground
		)

		// ... and set them as the value of these preferences. The 'attributes' and 'preferenceKeys'
		// arrays must be the same length, and the same order (i.e., the attribute at index 0 must
		// contain the value for the preference at index 0, and so on).
		val preferenceKeys = arrayOf(
			CustomTheme.COLOR_PRIMARY,
			CustomTheme.COLOR_ACCENT,
			CustomTheme.COLOR_LINE,
			CustomTheme.COLOR_SECTOR_LINE,
			CustomTheme.COLOR_VALUE_TEXT,
			CustomTheme.COLOR_MARKS_TEXT,
			CustomTheme.COLOR_BACKGROUND,
			CustomTheme.COLOR_READ_ONLY_TEXT,
			CustomTheme.COLOR_READ_ONLY_BACKGROUND,
			CustomTheme.COLOR_TOUCHED_TEXT,
			CustomTheme.COLOR_TOUCHED_MARKS_TEXT,
			CustomTheme.COLOR_TOUCHED_BACKGROUND,
			CustomTheme.COLOR_SELECTED_BACKGROUND,
			CustomTheme.COLOR_HIGHLIGHTED_TEXT,
			CustomTheme.COLOR_HIGHLIGHTED_MARKS_TEXT,
			CustomTheme.COLOR_HIGHLIGHTED_BACKGROUND,
			CustomTheme.COLOR_TEXT_ERROR,
			CustomTheme.COLOR_BACKGROUND_ERROR,
			CustomTheme.COLOR_EVEN_TEXT,
			CustomTheme.COLOR_EVEN_MARKS_TEXT,
			CustomTheme.COLOR_EVEN_BACKGROUND,
		)
		assert(attributes.size == preferenceKeys.size)
		val themeColors = themeWrapper.theme.obtainStyledAttributes(attributes)
		for (i in attributes.indices) {
			findPreference<ColorPickerPreference>(preferenceKeys[i])!!.onColorChanged(themeColors.getColor(i, Color.GRAY))
		}
		themeColors.recycle()
	}

	private fun createCustomThemeFromSingleColor(@ColorInt colorPrimary: Int) {
		val whiteContrast = ColorUtils.calculateContrast(colorPrimary, Color.WHITE)
		val blackContrast = ColorUtils.calculateContrast(colorPrimary, Color.BLACK)
		val isLightTheme = whiteContrast < blackContrast
		val colorAsHSL = FloatArray(3)
		ColorUtils.colorToHSL(colorPrimary, colorAsHSL)
		var tempHSL = colorAsHSL.clone()
		tempHSL[0] = (colorAsHSL[0] + 300f) % 360.0f
		val colorAccent = ColorUtils.HSLToColor(tempHSL)
		tempHSL = colorAsHSL.clone()
		tempHSL[2] += if (isLightTheme) -0.1f else 0.1f
		val colorPrimaryDark = ColorUtils.HSLToColor(tempHSL)
		val colorValueText = if (isLightTheme) Color.BLACK else Color.WHITE
		val colorBackground = if (isLightTheme) Color.WHITE else Color.BLACK
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_LINE)!!.onColorChanged(colorPrimaryDark)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_SECTOR_LINE)!!.onColorChanged(colorPrimaryDark)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_VALUE_TEXT)!!.onColorChanged(colorValueText)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_MARKS_TEXT)!!.onColorChanged(colorValueText)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_BACKGROUND)!!.onColorChanged(colorBackground)
		val colorReadOnlyText = colorOn(colorPrimary)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_READ_ONLY_TEXT)!!.onColorChanged(colorReadOnlyText)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_READ_ONLY_BACKGROUND)!!.onColorChanged(colorPrimary)
		val colorTouchedText = colorOn(colorAccent)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_TOUCHED_TEXT)!!.onColorChanged(colorTouchedText)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_TOUCHED_MARKS_TEXT)!!.onColorChanged(colorTouchedText)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_TOUCHED_BACKGROUND)!!.onColorChanged(colorAccent)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_SELECTED_BACKGROUND)!!.onColorChanged(colorPrimaryDark)
		val colorHighlightedText = colorOn(colorPrimary)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_HIGHLIGHTED_TEXT)!!.onColorChanged(colorHighlightedText)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_HIGHLIGHTED_MARKS_TEXT)!!.onColorChanged(colorHighlightedText)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_HIGHLIGHTED_BACKGROUND)!!.onColorChanged(colorPrimary)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_EVEN_TEXT)!!.onColorChanged(colorValueText)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_EVEN_MARKS_TEXT)!!.onColorChanged(colorValueText)
		// Default to transparent
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_EVEN_BACKGROUND)!!.onColorChanged(colorBackground)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_PRIMARY)!!.onColorChanged(colorPrimary)
		findPreference<ColorPickerPreference>(CustomTheme.COLOR_ACCENT)!!.onColorChanged(colorAccent)
	}

	/**
	 * @param background background color
	 * @return a color suitable for using "on" the given background color.
	 */
	@ColorInt
	private fun colorOn(@ColorInt background: Int): Int {
		val whiteContrast = ColorUtils.calculateContrast(Color.WHITE, background)
		val blackContrast = ColorUtils.calculateContrast(Color.BLACK, background)
		return if (whiteContrast >= blackContrast) Color.WHITE else Color.BLACK
	}

	override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String?) {
		if (key == "theme") {
			// This is the theme changing when custom_theme_ui_mode changes, and can be ignored.
			return
		}
		if (key == CUSTOM_THEME_UI_MODE) {
			setThemeCodeFromUiMode(sharedPreferences)
			val mode = sharedPreferences.getString(CUSTOM_THEME_UI_MODE, "system")
			ThemeUtils.sTimestampOfLastThemeUpdate = System.currentTimeMillis()
			if (mode == MODE_LIGHT) {
				val editor = sharedPreferences.edit()
				editor.putString("theme", "custom_light")
				editor.apply()
				AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
				return
			}
			if (mode == MODE_DARK) {
				val editor = sharedPreferences.edit()
				editor.putString("theme", "custom")
				editor.apply()
				AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
				return
			}
			AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
			return
		}
		if (key!!.contains("custom_theme_color")) {
			quantizeCustomAppColorPreferences()
		}
		updateThemePreview()
		ThemeUtils.sTimestampOfLastThemeUpdate = System.currentTimeMillis()
	}

	/**
	 * Edits the "theme" preference to "custom" or "custom_light" based on the value of
	 * "custom_theme_ui_mode" and applies the change.
	 *
	 * @param sharedPreferences shared preferences to edit
	 */
	private fun setThemeCodeFromUiMode(sharedPreferences: SharedPreferences) {
		val mode = sharedPreferences.getString(CUSTOM_THEME_UI_MODE, "system")
		sharedPreferences.edit().apply {
			if (mode == MODE_LIGHT) {
				putString("theme", "custom_light")
			} else if (mode == MODE_DARK) {
				putString("theme", "custom")
			}
		}.apply()
		ThemeUtils.sTimestampOfLastThemeUpdate = System.currentTimeMillis()
	}

	private fun quantizeCustomAppColorPreferences() {
		settings!!.colorPrimary = ThemeUtils.findClosestMaterialColor(settings!!.colorPrimary)
		settings!!.colorAccent = ThemeUtils.findClosestMaterialColor(settings!!.colorAccent)
	}

	override fun onResume() {
		super.onResume()
		settings!!.registerOnSharedPreferenceChangeListener(this)
	}

	override fun onPause() {
		super.onPause()
		settings!!.unregisterOnSharedPreferenceChangeListener(this)
	}
}
