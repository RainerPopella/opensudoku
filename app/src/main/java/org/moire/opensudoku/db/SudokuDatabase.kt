/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2024 by original authors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

@file:Suppress("HardCodedStringLiteral")

package org.moire.opensudoku.db

import android.content.ContentValues
import android.content.Context
import android.database.Cursor
import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteQueryBuilder
import android.util.Log
import org.moire.opensudoku.game.FolderInfo
import org.moire.opensudoku.game.SudokuBoard
import org.moire.opensudoku.game.SudokuGame
import org.moire.opensudoku.gui.PuzzleListFilter
import java.io.Closeable
import java.time.Instant
import java.util.LinkedList
import java.util.concurrent.Executors

/**
 * Wrapper around OpenSudoku's database.
 * Should not call the constructor from the application main thread as it may block it until DB is opened.
 * You have to close connection when you're done with the database.
 */
class SudokuDatabase(context: Context, readOnly: Boolean) : Closeable {
	private val openHelper: DatabaseHelper = DatabaseHelper(context)
	private val db: SQLiteDatabase = if (readOnly) openHelper.readableDatabase else openHelper.writableDatabase
	private val executorService = Executors.newSingleThreadExecutor()

	// FOLDER METHODS

	/**
	 * Returns list of puzzle folders.
	 */
	fun getFolderList(withCounts: Boolean = false): List<FolderInfo> {
		val qb = SQLiteQueryBuilder()
		val folderList: MutableList<FolderInfo> = LinkedList()
		qb.tables = FOLDERS_TABLE_NAME
		qb.query(db, null, null, null, null, null, null).forEach { cursor ->
			val folderInfo = cursor.folderInfo
			if (withCounts) {
				folderInfo.apply { supplyCounts(this) }
			}
			folderList.add(folderInfo)
		}
		return folderList
	}

	fun getFolderListWithCountsAsync(loadedCallback: (List<FolderInfo>) -> Unit) {
		executorService.execute {
			try {
				val folderInfo = getFolderList(true)
				loadedCallback(folderInfo)
			} catch (e: Exception) {    // this is unimportant, we can log an error and continue
				Log.e(javaClass.simpleName, "Error occurred while loading folder list with info.", e)
			}
		}
	}

	/**
	 * Returns the folder info.
	 *
	 * @param folderId Primary key of folder.
	 */
	fun getFolderInfo(folderId: Long): FolderInfo? {
		with(SQLiteQueryBuilder()) {
			tables = FOLDERS_TABLE_NAME
			query(db, null, Column.ID + "=" + folderId, null, null, null, null).use { cursor ->
				return@getFolderInfo if (cursor.moveToFirst()) cursor.folderInfo else null
			}
		}
	}

	/**
	 * Returns the folder info.
	 *
	 * @param folderName Name of the folder to get info.
	 */
	private fun getFolderInfo(folderName: String): FolderInfo? {
		with(SQLiteQueryBuilder()) {
			tables = FOLDERS_TABLE_NAME
			query(db, null, Column.NAME + "=?", arrayOf(folderName), null, null, null).use { cursor ->
				return@getFolderInfo if (cursor.moveToFirst()) cursor.folderInfo else null
			}
		}
	}

	/**
	 * Returns the full folder info - this includes count of games in particular states.
	 *
	 * @param folderId Primary key of folder.
	 * @return folder info
	 */
	fun getFolderInfoWithCounts(folderId: Long): FolderInfo {
		return getFolderInfo(folderId)!!.apply { supplyCounts(this) }
	}

	private fun supplyCounts(folder: FolderInfo) {
		val q = "SELECT ${Column.STATE}, COUNT(*) FROM $PUZZLES_TABLE_NAME WHERE ${Column.FOLDER_ID} = ${folder.id} GROUP BY 1"
		db.rawQuery(q, null).use { cursor ->
			while (cursor.moveToNext()) {
				val state = cursor.getInt(0)
				val count = cursor.getInt(1)
				folder.puzzleCount += count
				if (state == SudokuGame.GAME_STATE_COMPLETED) {
					folder.solvedCount = count
				} else if (state == SudokuGame.GAME_STATE_PLAYING) {
					folder.playingCount = count
				}
			}
		}
	}

	fun getFolderInfoWithCountsAsync(folderId: Long, loadedCallback: (FolderInfo) -> Unit) {
		executorService.execute {
			try {
				val folderInfo = getFolderInfoWithCounts(folderId)
				loadedCallback(folderInfo)
			} catch (e: Exception) {    // this is unimportant, we can log an error and continue
				Log.e(javaClass.simpleName, "Error occurred while loading full folder info.", e)
			}
		}
	}

	/**
	 * Inserts new puzzle folder into the database.
	 *
	 * @param name    Name of the folder.
	 * @param created Time of folder creation.
	 */
	fun insertFolder(name: String, createdOr0: Long): FolderInfo {
		val existingFolder = getFolderInfo(name)
		if (existingFolder != null) {
			return existingFolder
		}
		val created = if (createdOr0 > 0) createdOr0 else Instant.now().toEpochMilli()
		val values = ContentValues()
		values.put(Column.CREATED, created)
		values.put(Column.NAME, name)
		val rowId = db.insert(FOLDERS_TABLE_NAME, Column.ID, values)
		if (rowId < 0) {
			throw SQLException("Failed to insert folder '$name'.")
		}
		return FolderInfo(rowId, name, created)
	}

	/**
	 * Renames existing folder.
	 *
	 * @param folderId Primary key of folder.
	 * @param name     New name for the folder.
	 */
	fun renameFolder(folderId: Long, name: String) {
		val values = ContentValues()
		values.put(Column.NAME, name)
		db.update(FOLDERS_TABLE_NAME, values, Column.ID + "=" + folderId, null)
	}

	/**
	 * Deletes given folder.
	 *
	 * @param folderId Primary key of folder.
	 */
	fun deleteFolder(folderId: Long) {
		// delete all puzzles in folder we are going to delete
		db.delete(PUZZLES_TABLE_NAME, Column.FOLDER_ID + "=" + folderId, null)
		// delete the folder
		db.delete(FOLDERS_TABLE_NAME, Column.ID + "=" + folderId, null)
	}

	// PUZZLE METHODS

	/**
	 * Deletes given puzzle from the database.
	 */
	fun deletePuzzle(puzzleID: Long) {
		db.delete(PUZZLES_TABLE_NAME, Column.ID + "=" + puzzleID, null)
	}

	fun findPuzzle(originalValues: String, isEditMode: Boolean): SudokuGame? {
		SQLiteQueryBuilder().run {
			tables = PUZZLES_TABLE_NAME
			query(db, null, Column.ORIGINAL_VALUES + "=?", arrayOf(originalValues), null, null, null)
				.use { cursor -> if (cursor.moveToFirst()) return@findPuzzle extractSudokuGameFromCursorRow(cursor, isEditMode) }
		}
		return null
	}

	/**
	 * Returns sudoku game object.
	 *
	 * @param gameID Primary key of folder.
	 */
	@Suppress("SameReturnValue")
	internal fun getPuzzle(gameID: Long, isEditMode: Boolean): SudokuGame? {
		SQLiteQueryBuilder().run {
			tables = PUZZLES_TABLE_NAME
			query(db, null, Column.ID + "=" + gameID, null, null, null, null)
				.use { cursor -> if (cursor.moveToFirst()) return@getPuzzle extractSudokuGameFromCursorRow(cursor, isEditMode) }
		}
		return null
	}

	/**
	 * Returns a random sudoku puzzle ID.
	 *
	 * @param unsolvedOnly Whether to return only unsolved puzzles (GAME_STATE_NOT_STARTED and GAME_STATE_PLAYING) or all puzzles including solved ones.
	 */
	@Suppress("SameReturnValue", "HardCodedStringLiteral")
	internal fun getRandomPuzzleID(unsolvedOnly: Boolean): Long? {
		val query = if (unsolvedOnly){
			"SELECT ${Column.ID} FROM $PUZZLES_TABLE_NAME WHERE ${Column.STATE} != ${SudokuGame.GAME_STATE_COMPLETED} ORDER BY RANDOM() LIMIT 1"
		}else{
			"SELECT ${Column.ID} FROM $PUZZLES_TABLE_NAME WHERE ORDER BY RANDOM() LIMIT 1"
		}

		db.rawQuery(query, null).use { cursor ->
			if(cursor.moveToFirst()){
				return@getRandomPuzzleID cursor.id
			}
		}
		return null
	}

	internal fun insertPuzzle(newGame: SudokuGame): Long {
		val rowId = db.insert(PUZZLES_TABLE_NAME, null, newGame.contentValues)
		if (rowId < 0) {
			throw SQLException("Failed to insert puzzle.")
		}
		return rowId
	}

	internal fun updatePuzzle(game: SudokuGame): Int = db.update(PUZZLES_TABLE_NAME, game.contentValues, "${Column.ID}=${game.id}", null)

	internal fun resetAllPuzzles(folderID: Long) {
		val rowsCount = db.update(PUZZLES_TABLE_NAME, ContentValues().apply {
			putNull(Column.CELLS_DATA)
			put(Column.LAST_PLAYED, 0)
			put(Column.STATE, SudokuGame.GAME_STATE_NOT_STARTED)
			put(Column.MISTAKE_COUNTER, 0)
			put(Column.TIME, 0)
			put(Column.USER_NOTE, "")
			put(Column.COMMAND_STACK, "")
		}, "${Column.FOLDER_ID}=$folderID", null)

		if (rowsCount <= 0) {
			throw SQLException("Failed to update puzzles.")
		}
	}

	/**
	 * Returns list of sudoku game objects
	 *
	 * @param folderId Primary key of folder.
	 */
	internal fun getPuzzleListCursor(folderId: Long = 0L, filter: PuzzleListFilter? = null, sortOrder: String? = null): Cursor {
		val qb = SQLiteQueryBuilder()
		qb.tables = PUZZLES_TABLE_NAME
		if (folderId != 0L) {
			qb.appendWhere(Column.FOLDER_ID + "=" + folderId)
		}
		if (filter != null) {
			if (!filter.showStateCompleted) {
				qb.appendWhere(" and " + Column.STATE + "!=" + SudokuGame.GAME_STATE_COMPLETED)
			}
			if (!filter.showStateNotStarted) {
				qb.appendWhere(" and " + Column.STATE + "!=" + SudokuGame.GAME_STATE_NOT_STARTED)
			}
			if (!filter.showStatePlaying) {
				qb.appendWhere(" and " + Column.STATE + "!=" + SudokuGame.GAME_STATE_PLAYING)
			}
		}
		return qb.query(db, null, null, null, null, null, sortOrder)
	}

	fun folderExists(folderName: String): Boolean = getFolderInfo(folderName) != null

	override fun close() {
		executorService.shutdownNow()
		openHelper.close()
	}
}

internal fun extractSudokuGameFromCursorRow(cursor: Cursor, isEditMode: Boolean): SudokuGame? {
	val game: SudokuGame
	try {
		val cellsDataIndex = cursor.getColumnIndexOrThrow(Column.CELLS_DATA)
		val (board, dataVersion) = SudokuBoard.deserialize(
			cursor.getString(
				if (!cursor.isNull(cellsDataIndex)) cellsDataIndex else cursor.getColumnIndexOrThrow(Column.ORIGINAL_VALUES),
			),
			isEditMode
		)
		game = SudokuGame(board).apply {
			id = cursor.id
			created = cursor.getLong(cursor.getColumnIndexOrThrow(Column.CREATED))
			lastPlayed = cursor.getLong(cursor.getColumnIndexOrThrow(Column.LAST_PLAYED))
			state = cursor.getInt(cursor.getColumnIndexOrThrow(Column.STATE))
			mistakeCounter = cursor.getInt(cursor.getColumnIndexOrThrow(Column.MISTAKE_COUNTER))
			playingDuration = cursor.getLong(cursor.getColumnIndexOrThrow(Column.TIME))
			userNote = cursor.getString(cursor.getColumnIndexOrThrow(Column.USER_NOTE)) ?: ""
			folderId = cursor.getLong(cursor.getColumnIndexOrThrow(Column.FOLDER_ID))
			commandStack.deserialize(cursor.getString(cursor.getColumnIndexOrThrow(Column.COMMAND_STACK)), dataVersion)
		}
	} catch (e: Exception) {    // this shouldn't normally happen, db corrupted
		Log.e(DB_TAG, "Error extracting SudokuGame from cursor", e)
		return null
	}
	return game
}

internal fun Cursor.forEach(callback: ((Cursor) -> Unit)) {
	if (moveToFirst()) {
		while (!isAfterLast) {
			callback(this)
			moveToNext()
		}
	}
	close()
}

internal val Cursor.originalValues: String
	get() = getString(getColumnIndexOrThrow(Column.ORIGINAL_VALUES))

internal val Cursor.id: Long
	get() = getLong(getColumnIndexOrThrow(Column.ID))

internal val Cursor.name: String
	get() = getString(getColumnIndexOrThrow(Column.NAME)) ?: ""

internal val Cursor.created: Long
	get() = getLong(getColumnIndexOrThrow(Column.CREATED))

internal val Cursor.folderInfo: FolderInfo
	get() = FolderInfo(id, name, created)

const val DB_TAG = "SudokuDatabase"
