/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game.nextstep

import android.content.Context
import org.moire.opensudoku.R
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.CellGroup
import org.moire.opensudoku.game.HintHighlight
import org.moire.opensudoku.game.SudokuBoard

/** Strategy: Fish Basic
 *
 *  This strategy package 'Hidden Group' contains the following strategies:
 *  - x-wing
 *  - swordfish
 *  - jellyfish
 *  - squirmbag or starfish
 *  - whale
 *  - leviathan
 *
 */
open class NextStepFishBasicGroup(
	private val context: Context,
	private val board: SudokuBoard,
	private val hintLevel: HintLevels ): NextStep() {

	override fun search(): Boolean {
		// This methode is not implemented in this class!
		return false
	}

	/** Fish Basic Group
	 *
	 * Fish sizes from 2 to 7
	 *
	 *  	1n ... cyclone fish => use hidden single / naked single
	 *  	2n ... x-wing
	 *  	3n ... swordfish
	 *  	4n ... jellyfish
	 *  	5n ... squirmbag or starfish
	 *  	6n ... whale
	 *  	7n ... leviathan
	 *
	 *  ! The fish size 1 is not part of this strategy. it is already implemented
	 *  ! as Hidden Single and Naked Single.
	 *
	 * Messages:
	 *
	 * 	- Fish 2N - X-Wing, row (R) and column (C)
	 * 		"X-WING (R): {5}"
	 * 	    "- BaseSet -> (r2,r5)"
	 * 	    "- CoverSet -> (c5,c8)"
	 * 	    "-> remove {5} from (c5) -> [ r4c5 ]"
	 *
	 * 	- Fish 3N - Swordfish, row (R) and column (C)
	 *		 "Swordfish (R): {5}"
	 *		 "- BaseSet -> (r1,r5,r9)"
	 * 		 "- CoverSet -> (c2,c4,c8)"
	 * 		 "-> remove {5} from (c2) -> [ r3c2 ]"
	 * 		 "-> remove {5} from (c4) -> [ r7c4 ]"
	 * 		 "-> remove {5} from (c8) -> [ r7c8,r8c8 ]"
	 *		 "Swordfish (C): {9}"
	 * 		 "- BaseSet -> (c2,c4,c8)"
	 * 		 "- CoverSet -> (r3,r7,r9)"
	 * 		 "-> remove {9} from (r3) -> [ r3c6 ]"
	 * 		 "-> remove {9} from (r7) -> [ r7c5,r7c6 ]"
	 * 		 "-> remove {9} from (r9) -> [ r9c5,r9c6 ]"
	 *
	 * 	- Fish 4N - Jellyfish, row (R) and column (C)
	 * 			-> similar to (fish 3N) Swordfish
	 * 	- Fish 5N - Starfish, row (R) and column (C)
	 * 			-> similar to (fish 3N) Swordfish
	 * 	- Fish 6N - Wale, row (R) and column (C)
	 * 			-> similar to (fish 3N) Swordfish
	 * 	- Fish 7N - Leviathan, row (R) and column (C)
	 * 			-> similar to (fish 3N) Swordfish
	 *
	 *
	 * Logic:
	 *
	 * - the focus in this strategy is to work with one digit (candidate) X
	 *   	- X is a valid digit for a cell
	 * - the strategy in two orientations:
	 * 		- vertical
	 * 		- horizontal
	 * - basis is a grid of rows and columns ,
	 *   with nodes as cells with the candidate X with the dimension N
	 * - orientation: horizontal
	 * 		- the base set contains N rows
	 * 		- the cover set contains N columns
	 * 		- each row in the base set should contain:
	 * 			- in minimum 2 cells with the candidate X
	 * 			- in maximum N cells with the candidate X
	 * 		- based on the rows in the base set you will get columns for the cover set
	 * 		- the cover set should contain exactly N columns
	 * 		- each column in the cover set contains:
	 * 			- cells as part of the grid (intersection with the rows)
	 * 				- in minimum 2 cells with the candidate X
	 * 				- in maximum N cells with the candidate X
	 * 			- cells not part of the grid:
	 * 				- in minimum one cells with the candidate X,
	 * 			      which can be removed as part of the strategy
	 * TODO: check if the minimum of 2 is correct for all fish sizes!
	 * - orientation: vertical
	 * 	... change rows with columns and columns with rows
	 * 	... same rules
	 *
 	 */
	protected fun checkForFishBasicGroup(fishSize: Int): Boolean {

		if (fishSize !in listOf(2,3,4,5,6,7)) { return false } // allowed sizes

		val baseStrategyName =
			when(fishSize) {
				2 -> context.getString(R.string.hint_strategy_fish_basic_2n)
				3 -> context.getString(R.string.hint_strategy_fish_basic_3n)
				4 -> context.getString(R.string.hint_strategy_fish_basic_4n)
				5 -> context.getString(R.string.hint_strategy_fish_basic_5n)
				6 -> context.getString(R.string.hint_strategy_fish_basic_6n)
				7 -> context.getString(R.string.hint_strategy_fish_basic_7n)
				else -> "Fish (basic) with a size of $fishSize !"
			}
		var strategyName = baseStrategyName

		var orientationRow = false

		fun getHousesForBaseSet(): Array<CellGroup> {
			return if (orientationRow)
				board.getHousesRows()
			else
				board.getHousesColumns()
		}

		fun getHousesForCoverSet(): Array<CellGroup> {
			return if (orientationRow)
				board.getHousesColumns()
			else
				board.getHousesRows()
		}

		// loop over orientations Horizontal/Vertical
		forOrientation@ for (orientation in listOf(HouseTypes.ROW, HouseTypes.COL)) {
			if (orientation == HouseTypes.ROW) {
				orientationRow = true
				strategyName = "$baseStrategyName (R)"
			} else {
				orientationRow = false
				strategyName = "$baseStrategyName (C)"
			}
			// loop over all possible candidates
			forCandidateX@ for (candidateX in allowedDigits) {

				var houses = getHousesForBaseSet()

				// get possible coverSetLines
				var baseSetLines: ArrayList<List<Cell>> = ArrayList()
				var cntLinesWithX = 0
				for (house in houses) {
					var lineCells = house.cells.toList()
					lineCells = lineCells
						.filter { cell -> cell.value == 0 }
						.filter { cell -> candidateX in cell.centralMarks.marksValues }
					if ( lineCells.isNotEmpty() ) cntLinesWithX += 1
					if ( lineCells.size > 1 && lineCells.size <= fishSize ) baseSetLines.add(lineCells)
				}

				if ( cntLinesWithX <= fishSize) continue@forCandidateX // not enough lines with X
				if ( baseSetLines.size < fishSize) continue@forCandidateX // not enough base lines

				// create a list with all combinations of the lines
				val baseSets = combinations(baseSetLines,fishSize)

				// loop over the combinations
				forBaseSet@ for (baseSet in baseSets) {
					val baseSetCells = baseSet.flatMap { it }
					val baseSetLineCellsMap = baseSetCells
						.groupBy { if (orientationRow) it.rowIndex else it.columnIndex }
						.toSortedMap()
					val baseSetLineIndexList = baseSetLineCellsMap.keys.toList()
					val coverSetLineCellsMap = baseSetCells
						.groupBy { if (orientationRow) it.columnIndex else it.rowIndex }
						.toSortedMap()
					val coverSetLineIndexList = coverSetLineCellsMap.keys.toList()

					// only coverSets with fishSize lines are valid
					if ( coverSetLineIndexList.size != fishSize ) continue@forBaseSet

					// each coverSetLine should contain in minimum 2 cells
					for ((_,v) in coverSetLineCellsMap.iterator()) {
						if ( v.size < 2) continue@forBaseSet
					}

					// create list with cells where X can be removed
					var cellsToRemoveX = mutableListOf<Cell>()
					val coverSetHouses = getHousesForCoverSet()
					for (lineIndex in coverSetLineIndexList) {
						var cells = coverSetHouses[lineIndex].cells.toList()
						cells = cells
							.filter { cell -> cell.value == 0 }
							.filter { cell -> candidateX in cell.centralMarks.marksValues }
							.filter { cell -> cell !in baseSetCells }
						cellsToRemoveX.addAll(cells)
					}
					if ( cellsToRemoveX.isEmpty() ) continue@forBaseSet
					val cellsToRemoveXLineMap = cellsToRemoveX
						.groupBy { if (orientationRow) it.columnIndex else it.rowIndex }
						.toSortedMap()

					// *** fish found ***

					var regionCells = mutableListOf<Cell>()
					for (k in baseSetLineCellsMap.keys)
						if (orientationRow)
							regionCells.addAll(board.getHousesRows()[k].cells)
						else
							regionCells.addAll(board.getHousesColumns()[k].cells)
					for (k in coverSetLineCellsMap.keys)
						if (orientationRow)
							regionCells.addAll(board.getHousesColumns()[k].cells)
						else
							regionCells.addAll(board.getHousesRows()[k].cells)
					regionCells = regionCells.toSet().toList() as MutableList

					when (hintLevel) {
						HintLevels.LEVEL1 -> {
							nextStepText = strategyName
						}

						HintLevels.LEVEL2 -> {
							nextStepText =
								"$strategyName: {$candidateX}"
						}

						HintLevels.LEVEL3 -> {
							nextStepText = "$strategyName: {$candidateX}\n"
							nextStepText += context.getString(
								R.string.hint_strategy_fish_basic_base_set,
								( baseSetLineIndexList
									.map { n ->  if (orientationRow) "r${n+1}" else "c${n+1}" }
									.joinToString(",", "(", ")") )
							) + "\n"
							nextStepText += context.getString(
								R.string.hint_strategy_fish_basic_cover_set,
								( coverSetLineIndexList
									.map { n ->  if (orientationRow) "c${n+1}" else "r${n+1}" }
									.joinToString(",", "(", ")") )
							) + "\n"

							cellsToHighlight[HintHighlight.REGION] =
								regionCells.map { it.rowIndex to it.columnIndex }
							cellsToHighlight[HintHighlight.CAUSE] =
								baseSetCells.map { it.rowIndex to it.columnIndex }
						}

						HintLevels.LEVEL4 -> {
							nextStepText = "$strategyName: {$candidateX}\n"
							nextStepText += context.getString(
								R.string.hint_strategy_fish_basic_base_set,
								( baseSetLineIndexList
									.map { n ->  if (orientationRow) "r${n+1}" else "c${n+1}" }
									.joinToString(",", "(", ")") )
							) + "\n"
							nextStepText += context.getString(
								R.string.hint_strategy_fish_basic_cover_set,
								( coverSetLineIndexList
									.map { n ->  if (orientationRow) "c${n+1}" else "r${n+1}" }
									.joinToString(",", "(", ")") )
							) + "\n"
							for ((k,v) in cellsToRemoveXLineMap) {
								nextStepText += context.getString(
									R.string.hint_remove_candidate_from,
									"{$candidateX}",
									(if (orientationRow) "(c${k+1})" else "(r${k+1})") +
										" ➠ " + getCellsGridAddress(v)
								) + "\n"
							}
							cellsToHighlight[HintHighlight.REGION] =
								regionCells.map { it.rowIndex to it.columnIndex }
							cellsToHighlight[HintHighlight.CAUSE] =
								baseSetCells.map { it.rowIndex to it.columnIndex }
							cellsToHighlight[HintHighlight.TARGET] =
								cellsToRemoveX.map { it.rowIndex to it.columnIndex }
						}
					}
					if (fishSize > 4) // TODO remove after testing the strategy for fishSize 5,6,7
						nextStepText += "\nPlease send us this puzzle to optimize the strategy $strategyName !"
					return true
				} // forBaseSet@
			} // forCandidateX@
		} // forOrientation@
		return false
	}


	/** checkForXWing
	 *
	 * This function is currently not really in use.
	 * It was replaced by checkForFishBasicGroup with size 2.
	 * But for a limited time it will be called after the FishBasic function,
	 * to be sure the the new function is working
	 *
	 * TODO: remove this function
	 *
	 *
	 * 	- Check for x-wings, row (R) and column (C)
	 *          "X-Wing (R): {5} -> [ r2c1,r2c5,r5c1,r5c5 ]"
	 *          "-> remove {5} from [ r3c1 ]"
	 *          "-> remove {5} from [ r4c5,r7c5 ]"
	 *          "X-Wing (C): {1} -> [ r2c1,r5c1,r2c5,r5c5 ]"
	 *          "-> remove {1} from [ r2c4,r2c8 ]"
	 *          "-> remove {1} from [ r5c3,r5c7,r5c9 ]"
	 *
	 */
	protected fun checkForXWing(): Boolean {

		/** X-Wing
		 *
		 * We have an X-Wing:
		 *   - one candidate
		 *     - are on two rows (columns)
		 *       - in two cells
		 *         - and the cells are on the same columns (rows) in both rows (columns)
		 * In this case all the candidate can be removed from the other cells in both columns (rows).
		 *
		 * For the implementation I use baseSet for the lines with the two cells and
		 * coverSet for the lines were extra value can be removed. A switch will control if we
		 * are working on a row_x-wing or column_x-wing. So we need only one set of code for both
		 * x-wing orientations.
		 */

		var strategyName = "X-Wing (R/C)"
		val cells = board.cells
		var rowWing = true // rowWing vs. colWing

		fun getCell(baseSetLine: Int, coverSetLine: Int): Cell {
			return if (rowWing)
				cells[baseSetLine][coverSetLine]
			else
				cells[coverSetLine][baseSetLine]
		}

		forLineWing@ for (lineWing in listOf("ROW", "COL")) {
			strategyName = "X-Wing"
			if (lineWing == "ROW") {
				rowWing = true
				strategyName += " (R)"
			} else {
				rowWing = false
				strategyName += " (C)"
			}

			forCandidate@ for (candidate in 1..9) {

				forBaseSetLineOne@ for (baseSetLineOne in 0..<boardSize) {
					// search in baseSetLineOne for cells with the candidate
					val coverSetLineList = mutableListOf<Int>()
					for (coverSetLine in 0..<boardSize) {
						val cell = getCell(baseSetLineOne, coverSetLine)
						if (cell.value > 0) continue // cell has already a value -> next coverSetLine
						if (cell.centralMarks.hasNumber(candidate)) coverSetLineList.add(coverSetLine)
					}
					if (coverSetLineList.size != 2) continue@forBaseSetLineOne
					// baseSetLineOne has only two cells with this candidate
					forBaseSetLineTwo@ for (baseSetLineTwo in (baseSetLineOne + 1)..<boardSize) {
						// check if the candidate is present in the right coverSetLine position
						for (coverSetLine in coverSetLineList) {
							val cell = getCell(baseSetLineTwo, coverSetLine)
							if (cell.value > 0) continue@forBaseSetLineTwo // cell has already a value -> next baseSetLineTwo
							if (!cell.centralMarks.hasNumber(candidate)) continue@forBaseSetLineTwo
						}
						// check if there a additional candidates in the baseSetLineTwo
						for (coverSetLine in 0..<boardSize) {
							if (coverSetLine in coverSetLineList) continue
							val cell = getCell(baseSetLineTwo, coverSetLine)
							if (cell.value > 0) continue // cell has already a value -> next coverSetLine
							if (cell.centralMarks.hasNumber(candidate)) continue@forBaseSetLineTwo
						}
						// baseSetLineTwo found
						val wingCells = mutableListOf<Cell>()
						wingCells.add(getCell(baseSetLineOne, coverSetLineList[0]))
						wingCells.add(getCell(baseSetLineOne, coverSetLineList[1]))
						wingCells.add(getCell(baseSetLineTwo, coverSetLineList[0]))
						wingCells.add(getCell(baseSetLineTwo, coverSetLineList[1]))
						// baseSetLineOne and baseSetLineTwo = x-wing found -> check if action is needed
						val cellsToWorkOn = mutableMapOf<Int, MutableList<Cell>>()
						for (coverSetLine in coverSetLineList) {
							for (baseSetLine in 0..<boardSize) {
								if (baseSetLine == baseSetLineOne) continue
								if (baseSetLine == baseSetLineTwo) continue
								val cell = getCell(baseSetLine, coverSetLine)
								if (cell.value > 0) continue
								if (!cell.centralMarks.hasNumber(candidate)) continue

								val coverSetLineCells = cellsToWorkOn.getOrDefault(coverSetLine, mutableListOf())
								coverSetLineCells.add(cell)
								cellsToWorkOn[coverSetLine] = coverSetLineCells
							}
						}
						if (cellsToWorkOn.isEmpty()) continue@forBaseSetLineTwo
						val baseCells = hashSetOf<Cell>()
						wingCells.forEach {
							baseCells.addAll(it.row?.cells.orEmpty())
							baseCells.addAll(it.column?.cells.orEmpty())
						}
						val msgValues = "{$candidate}"
						val msgCells = getCellsGridAddress(wingCells)
						when (hintLevel) {
							HintLevels.LEVEL1 -> { nextStepText = strategyName }
							HintLevels.LEVEL2 -> { nextStepText = "$strategyName: $msgValues" }
							HintLevels.LEVEL3 -> {
								nextStepText = "$strategyName: $msgValues \u27A0 $msgCells"
								cellsToHighlight[HintHighlight.REGION] = baseCells.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.CAUSE] = wingCells.map { it.rowIndex to it.columnIndex }
							}
							HintLevels.LEVEL4 -> {
								nextStepText = "$strategyName: $msgValues \u27A0 $msgCells\n"
								cellsToWorkOn.forEach { entry ->
									nextStepText += context.getString(R.string.hint_remove_candidate_from,"{$candidate}",getCellsGridAddress(entry.value))
									nextStepText += "\n"
								}
								cellsToHighlight[HintHighlight.REGION] = baseCells.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.CAUSE] = wingCells.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.TARGET] = cellsToWorkOn.values.flatten().map { it.rowIndex to it.columnIndex }
							}
						}
						nextStepText += "\nPlease send us this puzzle to optimize the strategy X-WING!"
						return true
					}
				}
			}
		}
		return false
	}

}
