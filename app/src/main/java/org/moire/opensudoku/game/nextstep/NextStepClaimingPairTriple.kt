/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game.nextstep

import android.content.Context
import org.moire.opensudoku.R
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.HintHighlight
import org.moire.opensudoku.game.SudokuBoard
import kotlin.collections.addAll

/** Strategy: Claiming Pair / Claiming Triple
 */
class NextStepClaimingPairTriple(
	private val context: Context,
	private val board: SudokuBoard,
	private val hintLevel: HintLevels ): NextStep() {

	override fun search(): Boolean {
		return checkForClaimingPairTriple()
	}

	/** Strategy: Claiming Pair / Claiming Triple
	 *
	 * Claiming Pair and Claiming Triple
	 *  - Intersection Removal
	 * 	- LINE -> BOX Reduction / Interaction
	 *  - Locked Candidates Type 2
	 *
	 * Check if an candidate number exits in an row or column only in one box.
	 * If this is the case, these means that this candidate number will
	 * at the end in this box only in this row or column.
	 * You can delete all candidate with this number from the cells
	 * from all other rows or columns inside of the box.
	 *
	 * Message:
	 *
	 *		"Claiming Pair: {4} ➠ (b1,c3) / [ r1c3,r2c3 ]
	 *      " ✎ remove {4} from [ r1c2,r2c2 ]
	 * 		"Claiming Triple: {4} ➠ (b2,r1) / [ r1c4,r1c5,r1c6 ]
	 * 	    " ✎ remove {4} from [ r2c4,r3c4 ]

	 */
	private fun checkForClaimingPairTriple(): Boolean {

		var strategyName = "Claiming Pair/Triple"

		// loop over all house types , not BOX
		forHouseType@ for (houseType in HouseTypes.entries) {

			// get all houses for the house types
			val houseCellsArray = when (houseType) {
				HouseTypes.ROW -> board.getHousesRows()
				HouseTypes.COL -> board.getHousesColumns()
				HouseTypes.BOX ->
					//mCells.getHousesSectors()
					continue@forHouseType
			}

			// loop over all houses in the house type
			forHouse@ for (houseCells in houseCellsArray) {
				val regionCells = arrayListOf<Cell>()
				regionCells.addAll(houseCells.cells)

				// loop over all cells in the house and create a number-cell-map
				var numberCellsMap = mutableMapOf<Int, MutableList<Cell>>()
				forCellInHouse@ for (cell in houseCells.cells) {
					if (cell.value > 0) continue // already solved
					val marksValues = cell.centralMarks.marksValues
					for (number in marksValues) {
						val numberCells =
							numberCellsMap.getOrDefault(number, mutableListOf())
						numberCells.add(cell)
						numberCellsMap[number] = numberCells
					}
				}
				if (numberCellsMap.isEmpty()) continue@forHouse // all cells are solved

				// filter list by count < 4 and sort to make it easier for the user ...
				numberCellsMap = numberCellsMap
					.filter { it.value.size in listOf(2, 3) } as MutableMap<Int, MutableList<Cell>>
				if (numberCellsMap.isEmpty()) continue@forHouse // no cells found
				numberCellsMap = numberCellsMap
					.toSortedMap()
					.toList()
					.sortedBy { (_, value) -> value.size }
					.toMap() as MutableMap<Int, MutableList<Cell>>

				// loop over number sorted by count (single,pair,triple)
				forCount@ for ((number, cells) in numberCellsMap) {

					// all cells in the same box?
					val cellBoxes = cells.map { it.sectorIndex }.toSet()
					if (cellBoxes.size == 1) {
						val boxCells = cells[0].sector!!.cells.toList()
						val cellsToWorkOn = boxCells
							.filter { it !in cells }
							.filter { it.value == 0 }
							.filter { it.centralMarks.hasNumber(number) }
						strategyName = when (cells.size) {
							2 -> context.getString(R.string.hint_strategy_claiming_pair)
							3 -> context.getString(R.string.hint_strategy_claiming_triple)
							else-> "Claiming Group with size ${cells.size}"
						}
						if (cellsToWorkOn.isNotEmpty()) {
							regionCells.addAll(cells[0].sector!!.cells)
							val msgValues = "{$number}"
							val msgCells = "(${HouseTypes.BOX.houseAdr(cells[0])},${houseType.houseAdr(cells[0])}) / ${getCellsGridAddress(cells)}"
							when (hintLevel) {
								HintLevels.LEVEL1 -> { nextStepText = strategyName }
								HintLevels.LEVEL2 -> { nextStepText = "$strategyName: $msgValues" }
								HintLevels.LEVEL3 -> {
									nextStepText = "$strategyName: $msgValues \u27A0 $msgCells"
									cellsToHighlight[HintHighlight.REGION] = regionCells.map { it.rowIndex to it.columnIndex }
									cellsToHighlight[HintHighlight.CAUSE] = cells.map { it.rowIndex to it.columnIndex }
								}
								HintLevels.LEVEL4 -> {
									nextStepText = "$strategyName: $msgValues \u27A0 $msgCells\n"
									nextStepText += context.getString(R.string.hint_remove_candidate_from,"{$number}",getCellsGridAddress(cellsToWorkOn))
									cellsToHighlight[HintHighlight.REGION] = regionCells.map { it.rowIndex to it.columnIndex }
									cellsToHighlight[HintHighlight.CAUSE] = cells.map { it.rowIndex to it.columnIndex }
									cellsToHighlight[HintHighlight.TARGET] = cellsToWorkOn.map { it.rowIndex to it.columnIndex }
								}
							}
							return true
						}
					}
				}
			}
		}
		return false
	}

}

