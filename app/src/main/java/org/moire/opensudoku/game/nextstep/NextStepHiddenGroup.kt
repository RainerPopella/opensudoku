/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game.nextstep

import android.content.Context
import org.moire.opensudoku.R
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.HintHighlight
import org.moire.opensudoku.game.SudokuBoard

/** Strategy: Hidden Group
 *
 *  the strategy package 'Hidden Group' contains the following strategies:
 *  - Hidden Pair
 *  - Hidden Triple
 *  - Hidden Quad
 *
 */
open class NextStepHiddenGroup(
	private val context: Context,
	private val board: SudokuBoard,
	private val hintLevel: HintLevels ): NextStep() {

	override fun search(): Boolean {
		// This methode is not implemented in this class!
		return false
	}

	/** Hidden Pair / Hidden Triple / Hidden Quad
	 *
	 * If there are 2/3/4 cells in a house (in a row, column, or box) in which 2/3/4 candidates
	 * do not appear anywhere outside of those cells in the same house, those candidates
	 * must be inserted into those 2/3/4 cells. These cell should contain also other candidates.
	 * These other candidates can therefore be deleted from these two cells.
	 *
	 *  groupSize ... 2 = pair | 3 = triple | 4 = quad
	 *
	 * Messages:
	 *
	 * 		"Hidden Pair: {2,4} -> (c3) / [ r4c3,r5c3 ]"
	 * 		"-> remove 3 from [ r5c3 ]"
	 * 		"-> remove 5 from [ r4c3 ]"
	 * 		"-> remove 6 from [ r4c3,r5c3 ]"
	 * 		"-> remove 7 from [ r5c3 ]"
	 *
	 * 		"Hidden Triple: {2,4,5} -> (b7) / [ r8c2,r9c2,r9c3 ]"
	 * 		"-> remove 1 from [ r9c2 ]"
	 * 		"-> remove 6 from [ r9c3 ]"
	 *
	 *		"Hidden Quad: {2,4,5,8} -> (b8) / [ r7c5,r7c6,r8c5r8c6 ]"
	 *		"-> remove {3} from [ r7c5 ]"
	 *		"-> remove {6} from [ r7c5,r8c5 ]"
	 *
	 *
	 */
	protected fun checkForHiddenGroup(groupSize: Int): Boolean {

		if (groupSize < 2 || groupSize > 4) {
			error("groupSize must be between 2 and 4")
			return false
		}

		val strategyName =
			when(groupSize) {
				2 -> context.getString(R.string.hint_strategy_hidden_pair)
				3 -> context.getString(R.string.hint_strategy_hidden_triple)
				4 -> context.getString(R.string.hint_strategy_hidden_quad)
				else -> "Hidden Group with a size of $groupSize !"
			}

		// loop over all house types
		for (houseType in HouseTypes.entries) {

			// get all houses for the house types
			val houseCellsArray = when (houseType) {
				HouseTypes.ROW -> board.getHousesRows()
				HouseTypes.COL -> board.getHousesColumns()
				HouseTypes.BOX -> board.getHousesSectors()
			}

			// loop over all houses
			forHouse@ for (houseCells in houseCellsArray) {

				// create a list with needed solution values (= allowed candidates)
				var allowedCandidates = setOf(1, 2, 3, 4, 5, 6, 7, 8, 9)
				houseCells.cells
					.filter { it.value != 0 }
					.forEach { allowedCandidates = allowedCandidates.minus(it.value) }

				// enough candidates to have a hidden group ?
				if (allowedCandidates.size <= groupSize) continue@forHouse

				// create a list with all group combinations
				val groupCombinations = combinations(allowedCandidates.toList(), groupSize).toList()

				// loop over group combinations to find a hidden group
				forGroupCombination@ for (groupCombination in groupCombinations) {
					// loop over unsolved cells in a house
					// search for each number in the group the cells with such a candidate
					val numberCellsMap = mutableMapOf<Int, MutableList<Cell>>()
					for (cell in houseCells.cells) {
						if (cell.value != 0) continue // cell solved
						for (number in groupCombination) {
							if (cell.centralMarks.hasNumber(number)) {
								val numberCells = numberCellsMap.getOrDefault(number, mutableListOf())
								numberCells.add(cell)
								numberCellsMap[number] = numberCells
								// a number should not be in more cells than group size
								if (numberCells.size > groupSize) continue@forGroupCombination
							}
						}
					}

					// create a list of the cells found
					val cellsFound = numberCellsMap.values.flatten().toSet().toList()

					// the count of cells found must be equal to the group size
					if (cellsFound.size != groupSize) continue@forGroupCombination

					// get actions
					var removeCellsMap = mutableMapOf<Int, MutableList<Cell>>()
					for (cell in cellsFound) {
						val candidates = cell.centralMarks.marksValues.filter { it !in groupCombination }
						for (candidate in candidates) {
							val removeCells = removeCellsMap.getOrDefault(candidate, mutableListOf())
							removeCells.add(cell)
							removeCellsMap[candidate] = removeCells
						}
					}

					removeCellsMap = removeCellsMap.toSortedMap()

					if (removeCellsMap.isNotEmpty()) {
						// User can remove candidates
						val msgValues = "{${groupCombination.joinToString(",")}}"
						val msgCells = "(${houseType.houseAdr(houseCells.cells[0])}) / ${getCellsGridAddress(cellsFound)}"
						when (hintLevel) {
							HintLevels.LEVEL1 -> { nextStepText = strategyName }
							HintLevels.LEVEL2 -> { nextStepText = "$strategyName: $msgValues" }
							HintLevels.LEVEL3 -> {
								nextStepText =  "$strategyName: $msgValues \u27A0 $msgCells"
								cellsToHighlight[HintHighlight.REGION] = houseCells.cells.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.CAUSE] = cellsFound.map { it.rowIndex to it.columnIndex }
							}
							HintLevels.LEVEL4 -> {
								nextStepText = "$strategyName: $msgValues \u27A0 $msgCells\n"
								removeCellsMap.forEach { (number, cells) ->
									nextStepText += context.getString(R.string.hint_remove_candidate_from,"{$number}",getCellsGridAddress(cells))
									nextStepText += "\n"
								}
								cellsToHighlight[HintHighlight.REGION] = houseCells.cells.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.CAUSE] = cellsFound.map { it.rowIndex to it.columnIndex }
								cellsToHighlight[HintHighlight.TARGET] = removeCellsMap.values.flatten().map { it.rowIndex to it.columnIndex }
							}
						}
						return true
					}
				}
			}
		}
		return false
	}

}
