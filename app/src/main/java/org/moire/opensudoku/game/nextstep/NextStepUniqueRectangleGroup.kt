/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game.nextstep

import android.content.Context
import org.moire.opensudoku.R
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.HintHighlight
import org.moire.opensudoku.game.SudokuBoard

//TODO: remove translation block unique rectangle text resources

/** Strategy: Unique Rectangle Group
 *
 * The strategies in the group "Unique Rectangle" (UR) are based on four cells.
 * These cells are located in exactly two row, two columns and two boxes.
 * All four cells have the same two candidates A and B. (pure BVC/UR-Cells with UR-Pair)
 *
 * 			AB - AB
 * 		    |    |
 * 		    AB - AB
 *
 * This situation will lead into a puzzle with more than on solutions. (deadly pattern)
 * Due too the fact that we are dealing with puzzles with only one solution, we can use this
 * condition as a base for the strategies in this group.
 *
 * For all strategies there must be at least ONE PURE BVC of the four UR-Cells with only two
 * candidates (UR-Pair).
 *
 * The strategy package 'Unique Rectangle Group' contains the following strategies:
 *
 *  - Unique Rectangle Type 1
 *  - Unique Rectangle Type 2
 *  - Unique Rectangle Type 5
 *  - ...
 *
 */
open class NextStepUniqueRectangleGroup(
	private val context: Context,
	private val board: SudokuBoard,
	private val hintLevel: HintLevels ): NextStep() {

	override  fun search(): Boolean {
		// This methode is not implemented in this class!
		return false
	}


	/** Strategy: Unique Rectangle Type 1 / 2 / 5
	 *
	 *
	 *  - UR-Cells = list of the four corner cells of the Unique Rectangle
	 *  - nonPureCells = list of cells with one or more additional candidates
	 *  - AB = UR-Pair
	 *  - X  = one or more additional candidates
	 *  - x  = only one additional candidate
	 *
	 *
	 * ** Unique Rectangle Type 1  ... 3 pure cells plus 1 cell with UR-Pair
	 *    (UNIQUE CORNER)              and additional candidates
	 *
	 * 		AB - AB+X     	AB - AB    	   AB --- AB      AB+X - AB
	 * 		|    |          |    |         |      |       |      |
	 * 		AB - AB         AB - AB+X      AB+X - AB      AB --- AB
	 *
	 * 	 Action: If there are only one additional candidate then
	 * 	            this candidate is the solution value for the cell AB+X
	 * 	            BUT!
	 * 	            To make the handling in code easier for other types,
	 * 	            we use the two step approach. First remove the candidates
	 * 	            and then handle the "Naked Single"
	 * 	         else
	 * 	            the candidates AB can be removed from cells AB+X
	 * 	Messages:
	 *
	 *  	"Unique Rectangle Type 1:"
	 *  	" ⊞ UR-Pair ➠ {1,2}"
	 *  	" ⊞ UR-Cells ➠ [ r1c1,r1c1,r1c1,r1c1 ]"
	 *  	" ⊞ Extra-Candidates ➠ {3,4}"
	 * 		" ✎ remove candidates {1,2} from [ r1c1 ]"
	 *
	 *
	 * ** Unique Rectangle Type 2  ... 2 pure cells plus 2 cells with UR-Pair
	 *    (UNIQUE SIDE)                and 1 additional candidates
	 *                                 and these cells are in 1 row or 1 column
	 *
	 * 		AB+x - AB+x		AB --- AB		AB+x - AB		AB - AB+x
	 *      |      |		|      |		|      |		|    |
	 *  	AB --- AB		AB+x - AB+x		AB+x - AB		AB - AB+x
	 *
	 * 	 Action: In all cells seen by the two cells with the additional candidate,
	 * 	         the additional candidate can be removed.
	 *
	 *
	 * ** Unique Rectangle Type 5  ... 2 pure cells plus 2 cells with UR-Pair
	 *                                 and 1 additional candidates
	 *                                 and the cells are diagonal
	 *
	 * ** Unique Rectangle Type 5+  ... 1 pure cells plus 3 cells with UR-Pair
	 *                                  and 1 additional candidates
     *
	 *
	 */
	protected fun checkForUniqueRectangle125(): Boolean {

		var strategyName = context.getString(R.string.hint_strategy_unique_rectangle_type_n,"?")

		// create a list with all Bi-Values of the board
		var allBiValues = mutableListOf<List<Int>>()
		forRow@ for ( row in board.getHousesRows()) {
			forCellInRow@ for (cell in row.cells) {
				if (cell.value != 0) continue@forCellInRow // only empty cells
				if (cell.centralMarks.marksValues.size != 2) continue@forCellInRow // must have two candidates
				allBiValues.add(cell.centralMarks.marksValues)
			}
		}
		allBiValues = allBiValues.toSet().toList() as MutableList

		// check for each Bi-Value (UR-Candidates)
		forURCandidates@ for ( urCandidates in allBiValues ) {
			val urCellCombinations = generateAllURCellsCombination(urCandidates)

			// check each combination
			forCombination@ for ( urCells in urCellCombinations ) {
				if ( !urCellsCombinationAreValid(urCandidates,urCells) ) continue@forCombination

				// check the count of additional candidates
				val urCellsExtraCandidatesMap = mutableMapOf<Cell, List<Int>>()
				val urExtraCandidateCellsMap = mutableMapOf<Int, MutableList<Cell>>()
				for (urCell in urCells) {
					val cellExtraCandidates = urCell.centralMarks.marksValues
						.filter { it !in urCandidates }
					if ( cellExtraCandidates.isNotEmpty() ) {
						urCellsExtraCandidatesMap[urCell] = cellExtraCandidates
						for ( extraCandidate in cellExtraCandidates ) {
							val mapEntryCells = urExtraCandidateCellsMap.getOrDefault(extraCandidate,mutableListOf() )
							mapEntryCells.add(urCell)
							urExtraCandidateCellsMap[extraCandidate] = mapEntryCells
						}
					}
				}

				// check for Type 1,2,5,5+
				var urTypeFound = ""
				var extraCells = listOf<Cell>()
				var extraCandidates= listOf<Int>()
				var actionCells = listOf<Cell>()
				var actionCandidates= listOf<Int>()

				// check for Type 1
				if ( urTypeFound.isEmpty() ) {
					if ( urCellsExtraCandidatesMap.size == 1 ) {
						// one cell with extra candidates
						urTypeFound = "1"
						extraCells = urCellsExtraCandidatesMap.keys.toList()
						extraCandidates = urCellsExtraCandidatesMap[extraCells.first()]!!
						actionCells = extraCells
						actionCandidates = urCandidates
					}
				}

				// check for Type 2
				if ( urTypeFound.isEmpty() ) {
					if ( urExtraCandidateCellsMap.size == 1 ) {
						// one extra candidate
						extraCandidates = urExtraCandidateCellsMap.keys.toList()
						extraCells = urExtraCandidateCellsMap[extraCandidates.first()]!!
						if ( extraCells.size == 2 ) {
							// only two cells
							if ( (extraCells[0].rowIndex == extraCells[1].rowIndex)
								|| (extraCells[0].columnIndex == extraCells[1].columnIndex) ) {
								// both cells are in one row or one column
								actionCandidates = extraCandidates
								// UR Type 2 found
								// cells seen by extraCells, without extraCells
								actionCells = getCellsSeenByAll(extraCells)
									.filter { cell -> cell !in extraCells }
									.filter { cell -> cell.value == 0 }
									.filter { cell -> cell.centralMarks.hasNumber(extraCandidates.first()) }
								if ( actionCells.isNotEmpty() ) {
									// UR Type 2 with action found
									urTypeFound = "2"
								}
							}
						}
					}
				}
				if ( urTypeFound.isEmpty() ) continue@forCombination

				// UR found -> build Message

				// urCells , urCandidates
				// extraCells , extraCandidates
				// actionCells , actionCandidates

				strategyName = strategyName.replace( "?" , urTypeFound)

				var regionCells = mutableListOf<Cell>()
				val rowIndexList = urCells.map { it.rowIndex }.toSet().toList()
				for ( rowIndex in rowIndexList ) {
					val cells = urCells.filter { it.rowIndex == rowIndex }
					val mm = cells.map{ it.columnIndex }.toSortedSet()
					regionCells.addAll(cells.first().row!!.cells
						.filter { it.columnIndex >= mm.min() }
						.filter { it.columnIndex <= mm.max() }
					)
				}
				val columnIndexList = urCells.map { it.columnIndex }.toSet().toList()
				for ( columnIndex in columnIndexList ) {
					val cells = urCells.filter { it.columnIndex == columnIndex }
					val mm = cells.map{ it.rowIndex }.toSortedSet()
					regionCells.addAll(cells.first().column!!.cells
						.filter { it.rowIndex >= mm.min() }
						.filter { it.rowIndex <= mm.max() }
					)
				}
				regionCells = regionCells.toSet().toList() as MutableList<Cell>

				val causeCells = mutableListOf<Cell>()
				causeCells.addAll(urCells)

				var targetCells = mutableListOf<Cell>()
				targetCells.addAll(actionCells)
				targetCells = targetCells.toSet().toList() as MutableList<Cell>

				when (hintLevel) {
					HintLevels.LEVEL1 -> {
						nextStepText = strategyName
					}
					HintLevels.LEVEL2 -> {
						nextStepText = "$strategyName\n"
						nextStepText += context.getString(R.string.hint_strategy_unique_rectangle_pair
							,formatCellMarks(urCandidates)) + "\n"
					}
					HintLevels.LEVEL3 -> {
						nextStepText = "$strategyName\n"
						nextStepText += context.getString(R.string.hint_strategy_unique_rectangle_pair
							,formatCellMarks(urCandidates)) + "\n"
						nextStepText += context.getString(R.string.hint_strategy_unique_rectangle_cells
							,getCellsGridAddress(urCells)) + "\n"
						nextStepText += if ( extraCandidates.size == 1)
							context.getString(R.string.hint_strategy_unique_rectangle_candidate,
								formatCellMarks(extraCandidates),getCellsGridAddress(extraCells)) + "\n"
						else
							context.getString(R.string.hint_strategy_unique_rectangle_candidates,
								formatCellMarks(extraCandidates),getCellsGridAddress(extraCells)) + "\n"
						cellsToHighlight[HintHighlight.REGION] = regionCells
							.map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.CAUSE] = causeCells
							.map { it.rowIndex to it.columnIndex }
					}
					HintLevels.LEVEL4 -> {
						nextStepText = "$strategyName\n"
						nextStepText += context.getString(R.string.hint_strategy_unique_rectangle_pair
							,formatCellMarks(urCandidates)) + "\n"
						nextStepText += context.getString(R.string.hint_strategy_unique_rectangle_cells
							,getCellsGridAddress(urCells)) + "\n"
						nextStepText += if ( extraCandidates.size == 1)
							context.getString(R.string.hint_strategy_unique_rectangle_candidate,
								formatCellMarks(extraCandidates),getCellsGridAddress(extraCells)) + "\n"
						else
							context.getString(R.string.hint_strategy_unique_rectangle_candidates,
								formatCellMarks(extraCandidates),getCellsGridAddress(extraCells)) + "\n"
						nextStepText += if ( actionCandidates.size == 1)
							context.getString(R.string.hint_remove_candidate_from,
								formatCellMarks(actionCandidates),getCellsGridAddress(actionCells)) + "\n"
						else
							context.getString(R.string.hint_remove_candidates_from,
								formatCellMarks(actionCandidates),getCellsGridAddress(actionCells)) + "\n"
						cellsToHighlight[HintHighlight.REGION] = regionCells
							.map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.CAUSE] = causeCells
							.map { it.rowIndex to it.columnIndex }
						cellsToHighlight[HintHighlight.TARGET] = targetCells
							.map { it.rowIndex to it.columnIndex }
					}
				}
				return true
			} // forCellInRow@
		} // forRow@
		return false // nothing found
	}


	/** generateAllURCellsCombination()
	 *
	 * This function generates a list with all possible combinations of the cells in the board
	 * containing the BiValue. Is the combination of the cells is a valid UR-Cells combination
	 * must be checked in a further step.
	 *
	 */
	private fun generateAllURCellsCombination(biValue: List<Int>): List<List<Cell>> {
		// create a list with all Bi-Values
		val allPossibleURCells = mutableListOf<Cell>()
		forRow@ for ( row in board.getHousesRows()) {
			forCellInRow@ for (cell in row.cells) {
				if (cell.value != 0) continue@forCellInRow // only empty cells
				if (cell.centralMarks.marksValues.size < 2) continue@forCellInRow
				if ( !cell.centralMarks.marksValues.containsAll(biValue) ) continue@forCellInRow
				allPossibleURCells.add(cell)
			}
		}
		if ( allPossibleURCells.size < 4 ) return listOf()
		return combinations(allPossibleURCells,4).toList()
	}


	/** checkURCellsCombination
	 *
	 * This function checks if the urCells are valid or not.
	 */
	private fun urCellsCombinationAreValid(biValue: List<Int>, urCells: List<Cell>): Boolean {
		var cellFound = false
		for ( cell in urCells ) {
			if ( cell.centralMarks.marksValues.size == biValue.size
				&& cell.centralMarks.marksValues.toSet() == biValue.toSet() ) {
				cellFound = true
				break
			}
		}
		if ( !cellFound ) return false // no pure cell found
		val rowList = urCells.map { it.rowIndex }.toSet().toList()
		if ( rowList.size != 2 ) return false // not only in two row
		val colList = urCells.map { it.columnIndex }.toSet().toList()
		if ( colList.size != 2 ) return false // not only in two columns
		val boxList = urCells.map { it.sectorIndex }.toSet().toList()
		if ( boxList.size != 2 ) return false // not only in two boxes
		return true // valid
	}

}
