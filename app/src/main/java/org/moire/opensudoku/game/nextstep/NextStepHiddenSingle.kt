/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2009-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.game.nextstep

import android.content.Context
import org.moire.opensudoku.R
import org.moire.opensudoku.game.Cell
import org.moire.opensudoku.game.HintHighlight
import org.moire.opensudoku.game.SudokuBoard

/** Strategy: Hidden Single
 */
class NextStepHiddenSingle(
	private val context: Context,
	private val board: SudokuBoard,
	private val hintLevel: HintLevels ): NextStep() {

	override fun search(): Boolean {
		return checkForHiddenSingle()
	}

	/** Strategy: Hidden Single
	 *
	 * Find in the empty cells of a house a candidate with count 1 (single).
	 * The cell with this single candidate can contain more than one other candidates.
	 *
	 * Message:
	 * 			"Hidden Single: {5} -> (r5) / [ r5c2 ]"
	 * 			"-> enter value {5} into [ r5c2 ]"
	 *
	 * How to search:
	 *  Create a list with the count of all candidate numbers for a house.
	 *  Filter the counts for all numbers with the count 1.
	 *  It could be that there are more than one hidden single in a house.
	 *  We take the first one ...
	 */

	private fun checkForHiddenSingle(): Boolean {

		// loop over all house types
		for (houseType in HouseTypes.entries) {

			// get all houses for the house type
			val houseCellsArray = when (houseType) {
				HouseTypes.ROW -> board.getHousesRows()
				HouseTypes.COL -> board.getHousesColumns()
				HouseTypes.BOX -> board.getHousesSectors()
			}

			// loop over all houses
			for (houseCells in houseCellsArray) {
				val numberCountMap = mutableMapOf<Int, Int>()
				val numberCellMap = mutableMapOf<Int, Cell>()
				for (cell in houseCells.cells) {
					if (cell.value > 0) continue
					val candidates = cell.centralMarks.marksValues
					for (candidate in candidates) {
						val count = numberCountMap.getOrDefault(candidate, 0)
						numberCountMap[candidate] = count + 1
						numberCellMap[candidate] = cell
					}
				}
				val numberCountMapSingle = numberCountMap.filter { it.value == 1 }
				if (numberCountMapSingle.isNotEmpty()) {
					val strategyName = context.getString(R.string.hint_strategy_hidden_single)
					val candidate = numberCountMapSingle.keys.toIntArray()[0]
					val cell = numberCellMap[candidate]!!
					when(hintLevel) {
						HintLevels.LEVEL1 -> { nextStepText = strategyName }
						HintLevels.LEVEL2 -> { nextStepText = "$strategyName: {${candidate}}" }
						HintLevels.LEVEL3 -> {
							nextStepText =  "$strategyName: {${candidate}}" +
								" \u27A0 (${houseType.houseAdr(cell)}) / [${cell.gridAddress}]"
							cellsToHighlight[HintHighlight.REGION] = houseCells.cells
								.filterNot { it == cell }
								.map { it.rowIndex to it.columnIndex }
							cellsToHighlight[HintHighlight.CAUSE] = listOf(cell).map { it.rowIndex to it.columnIndex }
						}
						HintLevels.LEVEL4 -> {
							nextStepText =  "$strategyName: {${candidate}}" +
								" \u27A0 (${houseType.houseAdr(cell)}) / [${cell.gridAddress}]" + "\n" +
								context.getString(R.string.hint_enter_value_into,
									"{${candidate}}", "[${cell.gridAddress}]")
							cellsToHighlight[HintHighlight.REGION] = houseCells.cells
								.filterNot { it == cell }
								.map { it.rowIndex to it.columnIndex }
							cellsToHighlight[HintHighlight.CAUSE] = listOf(cell).map { it.rowIndex to it.columnIndex }
							cellsToHighlight[HintHighlight.TARGET] = listOf(cell).map { it.rowIndex to it.columnIndex }
						}
					}
					return true
				}
			}
		}
		return false
	}


}
