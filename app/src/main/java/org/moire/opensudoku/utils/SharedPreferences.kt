/*
 * This file is part of Open Sudoku - an open-source Sudoku game.
 * Copyright (C) 2025-2025 by Open Sudoku authors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.moire.opensudoku.utils

import android.content.Context
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import org.moire.opensudoku.R

class ColorPreferences(val context: Context) {
	val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

	val colorLine
		get() = sharedPreferences.getInt(CustomTheme.COLOR_LINE, ContextCompat.getColor(context, R.color.default_colorLine))
	val colorSectorLine
		get() = sharedPreferences.getInt(CustomTheme.COLOR_SECTOR_LINE, ContextCompat.getColor(context, R.color.default_colorSectorLine))
	val colorValueText
		get() = sharedPreferences.getInt(CustomTheme.COLOR_VALUE_TEXT, ContextCompat.getColor(context, R.color.default_colorValueText))
	val colorMarkText
		get() = sharedPreferences.getInt(CustomTheme.COLOR_MARKS_TEXT, ContextCompat.getColor(context, R.color.default_colorMarksText))
	val colorBackground
		get() = sharedPreferences.getInt(CustomTheme.COLOR_BACKGROUND, ContextCompat.getColor(context, R.color.default_colorBackground))
	val colorReadOnlyText
		get() = sharedPreferences.getInt(CustomTheme.COLOR_READ_ONLY_TEXT, ContextCompat.getColor(context, R.color.default_colorReadOnlyText))
	val colorReadOnlyBackground
		get() = sharedPreferences.getInt(CustomTheme.COLOR_READ_ONLY_BACKGROUND, ContextCompat.getColor(context, R.color.default_colorReadOnlyBackground))
	val colorTouchedText
		get() = sharedPreferences.getInt(CustomTheme.COLOR_TOUCHED_TEXT, ContextCompat.getColor(context, R.color.default_colorTouchedText))
	val colorTouchedMarksText
		get() = sharedPreferences.getInt(CustomTheme.COLOR_TOUCHED_MARKS_TEXT, ContextCompat.getColor(context, R.color.default_colorTouchedMarksText))
	val colorTouchedBackground
		get() = sharedPreferences.getInt(CustomTheme.COLOR_TOUCHED_BACKGROUND, ContextCompat.getColor(context, R.color.default_colorTouchedBackground))
	val colorSelectedCellFrame
		get() = sharedPreferences.getInt(CustomTheme.COLOR_SELECTED_BACKGROUND, ContextCompat.getColor(context, R.color.default_colorSelectedBackground))
	val colorHighlightedText
		get() = sharedPreferences.getInt(CustomTheme.COLOR_HIGHLIGHTED_TEXT, ContextCompat.getColor(context, R.color.default_colorHighlightedText))
	val colorMarkHighlightedText
		get() = sharedPreferences.getInt(CustomTheme.COLOR_HIGHLIGHTED_MARKS_TEXT, ContextCompat.getColor(context, R.color.default_colorHighlightedMarksText))
	val colorHighlightedBackground
		get() = sharedPreferences.getInt(CustomTheme.COLOR_HIGHLIGHTED_BACKGROUND, ContextCompat.getColor(context, R.color.default_colorHighlightedBackground))
	val colorEvenCellsText
		get() = sharedPreferences.getInt(CustomTheme.COLOR_EVEN_TEXT, ContextCompat.getColor(context, R.color.default_colorValueText))
	val colorEvenCellsMarksText
		get() = sharedPreferences.getInt(CustomTheme.COLOR_EVEN_MARKS_TEXT, ContextCompat.getColor(context, R.color.default_colorValueText))
	val colorEvenCellsBackground
		get() = sharedPreferences.getInt(CustomTheme.COLOR_EVEN_BACKGROUND, ContextCompat.getColor(context, R.color.default_colorBackground))
	val colorInvalidText
		get() = sharedPreferences.getInt(CustomTheme.COLOR_TEXT_ERROR, ContextCompat.getColor(context, R.color.default_colorTextError))
	val colorInvalidBackground
		get() = sharedPreferences.getInt(CustomTheme.COLOR_BACKGROUND_ERROR, ContextCompat.getColor(context, R.color.default_colorBackgroundError))
}
