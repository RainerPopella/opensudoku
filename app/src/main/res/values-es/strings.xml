<?xml version="1.0" encoding="utf-8"?>
<!--
  ~ This file is part of Open Sudoku - an open-source Sudoku game.
  ~ Copyright (C) 2009-2025 by Open Sudoku authors.
  ~
  ~ This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the
  ~ Free Software Foundation, either version 3 of the License, or (at your option) any later version.
  ~
  ~ This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
  ~ FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
  ~
  ~ You should have received a copy of the GNU General Public License along with this program.  If not, see <https://www.gnu.org/licenses/>.
  --><resources>
    <string name="app_name">Open Sudoku</string>
    <string name="edit_puzzle">Editar sudoku</string>
    <string name="version">Open Sudoku versión %s</string>
    <string name="current_developers_label">Desarrollador principal (desde la versión 4)</string>
    <string name="old_developers_label">Antiguos desarrolladores</string>
    <string name="localization_contributors_label">Traducciones</string>
    <string name="close">Cerrar</string>
    <string name="clear">Borrar</string>
    <string name="save">Guardar</string>
    <string name="discard">Descartar</string>
    <string name="get_more_puzzles_online">Obtener mas puzles en línea</string>
    <string name="name">Nombre:</string>
    <string name="note">Notas:</string>
    <string name="edit_note">Editar notas</string>
    <string name="add_folder">Añadir carpeta</string>
    <string name="import_title">Importar</string>
    <string name="about">Sobre</string>
    <string name="error">Error</string>
    <string name="rename_folder">Renombrar carpeta</string>
    <string name="delete_folder">Borrar carpeta</string>
    <string name="rename_folder_title">Renombrar %s</string>
    <string name="delete_folder_title">Borrar %s</string>
    <string name="delete_folder_confirm">¿Estás completamente seguro de que quieres borrar esta carpeta?</string>
    <plurals name="n_puzzles">
        <item quantity="one">Un puzle</item>
        <item quantity="many">%d de puzles</item>
        <item quantity="other">%d puzles</item>
    </plurals>
    <string name="all_solved">todos resueltos</string>
    <string name="n_unsolved">%d sin resolver</string>
    <string name="n_playing">%d jugando</string>
    <string name="solved">Resuelto</string>
    <string name="playing">Jugando</string>
    <string name="not_started">Sin comenzar</string>
    <string name="last_played_at">Jugado por útima vez %s</string>
    <string name="created_at">Creado %s</string>
    <string name="today_at_time">a las %s</string>
    <string name="yesterday_at_time">ayer a las %s</string>
    <string name="on_date">en %s</string>
    <string name="add_puzzle">Añadir sudoku</string>
    <string name="filter">Filtrar</string>
    <string name="filter_by_game_state">Filtrar por estado</string>
    <string name="folders">Carpetas</string>
    <string name="delete_puzzle_confirm">¿Estás seguro de que quieres borrar este puzle?</string>
    <string name="reset_puzzle_confirm">¿Estás seguro de que quieres reiniciar este puzle?</string>
    <string name="reset_puzzle">Reiniciar puzle</string>
    <string name="delete_puzzle">Eliminar puzle</string>
    <string name="filter_active">Mostrar sólo: %s</string>
    <string name="undo">Deshacer</string>
    <string name="clear_all_marks">Eliminar todas las anotaciones</string>
    <string name="fill_in_marks">Autorellenar anotaciones primarias</string>
    <string name="restart">Reiniciar</string>
    <string name="congrats">Felicidades, has resuelto el puzle!</string>
    <string name="congrats_duration">\nDuración: %s</string>
    <string name="congrats_mistakes">\nRecuento de errores: %d</string>
    <string name="restart_confirm">¿Estas seguro de querer reiniciar el juego?</string>
    <string name="well_done">¡Bien hecho!</string>
    <string name="clear_all_marks_confirm">¿Estas seguro de querer borrar todas las anotaciones?</string>
    <string name="settings">Ajustes</string>
    <string name="game_settings">Ajustes del juego</string>
    <string name="board_view_settings">Ayudas en el juego</string>
    <string name="highlight_directly_wrong_values">Resaltar valores erroneos</string>
    <string name="highlight_indirectly_wrong_values">Resaltar valores indirectamente erróneos</string>
    <string name="highlight_wrong_values_summary">Resaltar los valores que no coincidan con la solución final del puzle (se ignoran las notas).</string>
    <string name="input_methods">Métodos de entrada</string>
    <string name="popup">Ventana emergente</string>
    <string name="insert_on_tap">Insertar al tocar</string>
    <string name="select_on_tap">Seleccionar al tocar</string>
    <string name="move_right_on_insert">Mover a la derecha</string>
    <string name="move_right_on_insert_summary">Mueve la selección de la celda una a la derecha cuando se presiona un botón.</string>
    <string name="select_on_tap_abbr">Seleccionar al tocar</string>
    <string name="popup_abbr">VE</string>
    <string name="insert_on_tap_abbr">Toque para insertar</string>
    <string name="welcome">Bienvenido</string>
    <string name="hint">Sugerencias</string>
    <string name="help">Ayuda</string>
    <string name="that_is_all">Eso es todo</string>
    <string name="first_run_hint">Hay varios modos de entrada que puedes utilizar para completar los puzles. Para ayudarte a familiarizarte con estos modos, se mostrarán varias consejos a medida que vayas avanzando por ellos. Puedes desactivar estas sugerencias en la configuración del juego.</string>
    <string name="im_popup_hint">Ahora te encuentras en el modo de entrada <i>ventana emergente</i>. Elige cualquier celda con un toque para editar su contenido. Fíjate en el botón de la esquina inferior derecha. Puedes utilizar este botón para cambiar a otro modo de entrada.</string>
    <string name="im_insert_on_tap_hint">Ahora te encuentras en el modo de entrada <i>insertar en el toque de celda</i>. En primer lugar, selecciona cualquier botón pulsándolo. A continuación, sólo tienes que pulsar sobre las celdas en las que quieres que se aplique la clave seleccionada. Presiona el botón de marcas de lápiz para activar la edición de anotaciones de la celda.</string>
    <string name="im_select_on_tap_hint">Ahora te encuentras en el modo de entrada <i>seleccionar al tocar la celda</i>. Los botones actúan como un teclado estándar. Primero debes seleccionar la celda tocándola y, a continuación, pulsar el botón para modificar la celda seleccionada. Presiona el botón de marcas de lápiz para activar la edición de anotaciones de la celda.</string>
    <string name="im_disable_modes_hint">Por favor, recuerda que no tienes que utilizar todos los modos de entrada. Puedes desactivar cualquiera de ellos en la configuración del juego. También puedes usar el teclado y el trackball para controlar el juego. Si quieres introducir anotaciones usando un teclado, presiona el número junto con la tecla alt (anotaciones primarias) o mayúsculas (anotaciones secundarias, si están activadas).</string>
    <string name="help_text"><b>Controles del teclado</b> Utiliza el trackball para navegar hasta la celda deseada y, a continuación, pulsa un número en el teclado para rellenar el valor. Si quieres editar la anotación de la celda, presiona el número junto con la tecla alt (anotaciones primarias) o mayúsculas (anotaciones secundarias, si están activadas). \n<b>Modos de entrada</b> Puedes cambiar entre los diferentes modos de entrada utilizando el botón situado en la esquina inferior derecha de la pantalla. También puedes desactivar cualquiera de estos modos en la configuración del juego y quedará oculto durante el mismo. \n<b>Modo de entrada de ventana emergente</b> Se muestra un cuadro de diálogo emergente al tocar la celda. Puedes editar el contenido de la misma mediante dicho cuadro. \n<b>Modo insertar en el toque de celda</b> Selecciona cualquier botón pulsándolo y, a continuación, pulsa la celda en la que quieres que se inserte el número seleccionado. \n<b>Modo seleccionar al tocar la celda</b> Los botones actúan como un teclado numérico estándar. \nEn cualquier modo, presiona el botón de marcas de lápiz para activar la edición de anotaciones de la celda..</string>
    <string name="game">Juego</string>
    <string name="show_hints">Mostrar sugerencias</string>
    <string name="show_hints_summary">Mostar sugerencias que te guían con los controles de la aplicación.</string>
    <string name="show_time">Mostrar tiempo</string>
    <string name="show_time_summary">Muestra el tiempo durante la resolución del puzle.</string>
    <string name="puzzle_created">Nuevo puzle guardado</string>
    <string name="puzzle_updated">Puzle actualizado</string>
    <string name="checking_duplicates">Revisando los puzles de Sudoku existentes…</string>
    <plurals name="target_folders">
        <item quantity="one">Carpeta de destino: %s.</item>
        <item quantity="many">Carpetas de destino: %s.</item>
        <item quantity="other">Carpetas de destino: %s.</item>
    </plurals>
    <string name="no_puzzles_found">No se han encontrado puzles</string>
    <string name="unknown_import_error">Error desconocido importando puzles, la importación se ha cancelado.</string>
    <string name="invalid_format">Formato no válido, la importación se ha cancelado</string>
    <string name="screen_border_size">Tamaño del borde de la pantalla</string>
    <string name="screen_border_size_summary">Algunas pantallas no reaccionan a los toques en los bordes de las mismas.
		Esto podría ayudar en ese caso.
	</string>
    <string name="highlight_touched_cell">Resaltar celda al tocar</string>
    <string name="highlight_touched_cell_summary">Resalta la fila y la columna de la celda seleccionada.</string>
    <string name="export">Exportar</string>
    <string name="export_folder">Exportar carpeta</string>
    <string name="export_all_folders">Exportar todas las carpetas</string>
    <string name="exporting">Desde la carpeta \'%s\'…</string>
    <string name="exported_num">Puzles exportados %1$d/%2$d…</string>
    <string name="unknown_export_error">Error desconocido exportando puzles</string>
    <string name="highlight_completed_values">Resaltar completados</string>
    <string name="highlight_completed_values_summary">Resalta los botones de los números que ya se han introducido nueve veces. También parpadean los números en el tablero.</string>
    <string name="theme">Tema</string>
    <string name="select_theme">Seleccionar tema</string>
    <string name="puzzles_have_been_exported">Los puzles se han exportado en %s</string>
    <!-- localization of default folder names -->
	<string name="difficulty_hard">Difícil</string>
    <string name="difficulty_medium">Medio</string>
    <string name="difficulty_easy">Fácil</string>
    <string name="show_number_totals">Mostrar totales</string>
    <string name="show_number_totals_summary">Muestra el total de veces que se ha colocado cada número.</string>
    <string name="what_is_new">Novedades</string>
    <string name="enable_double_marks">Anotaciones dobles</string>
    <string name="enable_double_marks_summary">Habilita dos tipos de anotaciones: centrales (como anotaciones primarias) y de esquina (como secundarias).</string>
    <string name="fill_in_marks_summary">Habilitar el elemento del menú que permite que las anotaciones primarias se cubran de forma automática.</string>
    <string name="set_checkpoint">Crear punto de control</string>
    <string name="undo_to_checkpoint">Deshacer al punto de control</string>
    <string name="undo_to_checkpoint_confirm">¿Estás seguro de querer deshacer todas las acciones
		desde el último punto de control?
	</string>
    <string name="highlight_similar_cells">Resaltar celdas similares</string>
    <string name="highlight_similar_cells_summary">Resaltar celdas con valores similares.</string>
    <string name="contributors_label">Colaboradores</string>
    <string name="bidirectional_selection">Selección bidireccional</string>
    <string name="bidirectional_selection_summary">En el método de entrada de número simple, la selección de una celda no editable automáticamente selecciona el
		botón correspondiente y viceversa.
	</string>
    <string name="theme_paper">Papel</string>
    <string name="theme_graph_paper">Papel cuadriculado</string>
    <string name="theme_light">Claro</string>
    <string name="theme_paper_light">Papel claro</string>
    <string name="theme_graph_paper_light">Papel cuadriculado claro</string>
    <string name="theme_high_contrast">Alto contraste</string>
    <string name="theme_inverted_high_contrast">Alto contraste invertido</string>
    <string name="theme_custom">Tema personalizado</string>
    <string name="screen_custom_theme">Colores del tema personalizado</string>
    <string name="screen_custom_theme_summary_disabled">Escoge \"Tema personalizado\" en el ajuste anterior \"Tema\" para habilitar esta sección.</string>
    <string name="default_colorLine">Línea</string>
    <string name="default_colorSectorLine">Línea del sector</string>
    <string name="default_colorValueText">Texto</string>
    <string name="default_colorMarksText">Texto de las notas</string>
    <string name="default_colorBackground">Fondo primario</string>
    <string name="solve_puzzle">Resolver puzle</string>
    <string name="solve_puzzle_confirm">¿Estás seguro de que quieres ver la solución?</string>
    <string name="used_solver">¡Has usado el solucionador para resolver el puzle!</string>
    <string name="puzzle_has_no_solution">Este puzle no tiene solución.</string>
    <string name="puzzle_has_multiple_solutions">Este es un puzle no válido porque tiene múltiples soluciones.</string>
    <string name="puzzle_already_exists">Este puzle es válido pero ya existe en la carpeta %s.</string>
    <string name="hint_confirm">Esto introducirá el valor correcto en la celda seleccionada. ¿Quieres continuar?</string>
    <string name="you_cant_modify_this_cell">No puedes obtener una pista para esa celda.</string>
    <string name="check_validity">Comprobar si tiene solución</string>
    <string name="puzzle_solvable">Este puzle tiene solución.</string>
    <string name="copy_from_existing_theme">Copiar del tema existente</string>
    <string name="theme_default">Oscuro</string>
    <string name="theme_latte">Latte de caramelo</string>
    <string name="theme_espresso">Café exprés</string>
    <string name="theme_sunrise">Amanecer</string>
    <string name="theme_honeybee">Miel de abeja</string>
    <string name="theme_crystal">Cristal</string>
    <string name="theme_midnight_blue">Azul medianoche</string>
    <string name="dark_mode_title">Modo de interfaz</string>
    <string name="theme_emerald">Esmeralda</string>
    <string name="theme_forest">Bosque</string>
    <string name="theme_amethyst">Amatista</string>
    <string name="theme_ruby">Rubí</string>
    <string name="app_color_primary_title">Color primario de la aplicación</string>
    <string name="app_color_primary_summary">Barra de estado, botones, tintado</string>
    <string name="app_color_accent_title">Color de acento de la aplicación</string>
    <string name="app_color_accent_summary">Conmutadores y otros widgets</string>
    <string name="highlight_similar_marks">Resaltar notas similares</string>
    <string name="highlight_similar_marks_summary">Resaltar celdas con valores de notas similares a la celda seleccionada</string>
    <string name="resume">Continuar</string>
    <string name="puzzle_lists">Lista de Sudokus</string>
    <string name="undo_to_before_mistake">Deshacer a antes del error</string>
    <string name="undo_to_before_mistake_confirm">¿Seguro que quieres deshacer todas las acciones desde el último estado solucionable?</string>
    <string name="remove_marks_title">Eliminar anotaciones al introducir un número</string>
    <string name="remove_marks_summary">Eliminar automáticamente las anotaciones de la misma columna, fila y celda al ingresar un número.</string>
    <string name="create_from_single_color">Crear desde un solo color</string>
    <string name="show_puzzle_lists_on_startup_title">Mostrar la lista de Sudokus al inicio</string>
    <string name="show_puzzle_lists_on_startup_summary">Mostrar la lista de Sudokus al inicio y omitir la pantalla de título.</string>
    <string name="sort">Ordenar</string>
    <string name="sort_puzzles_by">Ordenar puzles por</string>
    <string name="sort_creation_date">Fecha de creación</string>
    <string name="sort_play_time">Tiempo de juego</string>
    <string name="sort_last_played">Último jugado</string>
    <string name="pasted_from_clipboard">Pegado del portapapeles</string>
    <string name="copied_to_clipboard">Copiado al portapapeles</string>
    <string name="invalid_puzzle_format">Formato de puzle no válido</string>
    <string name="invalid_mime_type">Tipo MIME no válido, comprueba el contenido del portapapeles</string>
    <string name="reset_all_puzzles">Reiniciar todo</string>
    <string name="reset_all_puzzles_confirm">¿Estas seguro de querer reiniciar todos los puzles de esta carpeta?</string>
    <string name="fill_all_marks">Rellenar notas primarias con todos los valores</string>
    <string name="dark_mode_light">Modo de luz</string>
    <string name="dark_mode_dark">Modo oscuro</string>
    <string name="dark_mode_system">Usar el modo claro/oscuro del sistema</string>
    <string name="disabled_by_theme">El tema seleccionado establece el modo</string>
    <string name="theme_custom_light">Tema de luz personalizado</string>
    <string name="pref_border_title">Bordes de celda</string>
    <string name="pref_border_summary">Colores de borde alrededor de celdas y cuadros de 3x3</string>
    <string name="pref_touched_title">Células tocadas</string>
    <string name="pref_touched_summary">Celdas en la cruz de selección</string>
    <string name="pref_selected_title">Celda seleccionada</string>
    <string name="pref_selected_summary">El indicador de enfoque de la celda seleccionada</string>
    <string name="pref_highlighted_title">Destacado</string>
    <string name="pref_highlighted_summary">Celdas idénticas a la celda seleccionada</string>
    <string name="pref_error_title">Valores no válidos</string>
    <string name="pref_error_summary">Celdas que contienen valores no válidos</string>
    <string name="pref_even_title">Células uniformes</string>
    <string name="pref_even_summary">Celdas en cajas pares de 3x3</string>
    <string name="do_you_want_to_save_anyway">Presione OK si desea guardarlo de todos modos.</string>
    <string name="do_you_want_to_save">Presione OK para guardar el puzle.</string>
    <string name="found_n_puzzles">Encontrados %d puzles…</string>
    <string name="folder_already_exists">La carpeta con este nombre ya existe. Utilice un nombre de carpeta único.</string>
    <string name="sudoku_board_widget">Widget de tablero de Sudoku</string>
    <string name="folder_name">Nombre de la carpeta</string>
    <string name="corner_mark">Nota en la esquina</string>
    <string name="puzzle_selection_position">Posición de selección del puzle %d</string>
    <string name="central_marks">Nota en el centro</string>
    <string name="cell_value">Valor de celda</string>
    <string name="user_note_value">Valor de nota de usuario</string>
    <string name="puzzle_value_current">Valores actuales</string>
    <string name="copy_to_clp">Copiar al portapapeles</string>
    <string name="puzzle_value_initial">Valores iniciales</string>
    <string name="sound">Sonido</string>
    <string name="scan_puzzle">Escanear rompecabezas</string>
    <string name="whole_cells">Celdas</string>
    <string name="off">Desactivar</string>
    <string name="values_only">Valores</string>
    <string name="hint_no_step_found">No se ha encontrado ningún paso utilizando las estrategias aplicadas :-(</string>
    <string name="solve_cell">Resolver la celda actual</string>
    <string name="sudoku_puzzle">Puzle sudoku </string>
    <string name="permission_denied">Permiso denegado</string>
    <string name="cannot_start_web_activity_please_open_the_webpage_manually">No puede iniciar la actividad web. Por favor, abre la página web manualmente: %1$s</string>
    <plurals name="imported_new_puzzles_count">
        <item quantity="one">Importado un nuevo puzle.</item>
        <item quantity="many">Importados %d de nuevos puzles.</item>
        <item quantity="other">Importados %d nuevos puzles.</item>
    </plurals>
    <plurals name="skipped_already_existing_puzzles_count">
        <item quantity="one">Omitido un puzle ya existente.</item>
        <item quantity="many">Omitidos %d de puzles ya existentes.</item>
        <item quantity="other">Omitidos %d puzles ya existentes.</item>
    </plurals>
    <plurals name="updated_existing_puzzles_with_saved_in_progress_games_count">
        <item quantity="one">Actualizado un puzle existente con una partida guardada en curso.</item>
        <item quantity="many">Actualizados %d de puzles existentes con partidas guardadas en curso.</item>
        <item quantity="other">Actualizados %d puzles existentes con partidas guardadas en curso.</item>
    </plurals>
    <string name="privacy_policy">Política de privacidad</string>
    <string name="import_strategy_title">Estrategia de importación</string>
    <string name="import_strategy_summary">Define la estrategia de importación</string>
    <string name="import_strategy_no_duplicates">Sin duplicados: si el puzle ya existe en alguna carpeta no se importará</string>
    <string name="import_strategy_clean">Limpio: importar el puzle sin progreso ni historial</string>
    <string name="import_strategy_sync">Sincronizado: se utiliza para clonar los puzles, el progreso y el historial</string>
    <string name="import_strategy_used">Estrategia de importación utilizada: %1$s</string>
    <string name="import_strategy_no_duplicates_name">Sin duplicados</string>
    <string name="import_strategy_clean_name">Limpio</string>
    <string name="import_strategy_sync_name">Sincronizado</string>
    <string name="hint_level_2_title">estrategia y valores</string>
    <string name="hint_enter_value_into">✎ introducir valor %1$s en %2$s</string>
    <string name="hint_strategy_naked_single">Sencillo obvio</string>
    <string name="hint_strategy_hidden_single">Sencillo oculto</string>
    <string name="hint_strategy_naked_quad">Cuarteto obvio</string>
    <string name="hint_strategy_locked_pair">Pareja bloqueada</string>
    <string name="hint_strategy_locked_triple">Trío bloqueado</string>
    <string name="hint_strategy_full_house">Último dígito</string>
    <string name="no_mistakes_found">El puzle aún se puede resolver, no se han encontrado errores.</string>
    <string name="hint_level_dialog_title">Elige el nivel de pistas</string>
    <string name="hint_level_1_title">sólo estrategia</string>
    <string name="hint_level_3_title">estrategia, valores y celdas</string>
    <string name="hint_level_4_title">estrategia, valores, celdas y acciones</string>
    <string name="hint_strategy_naked_pair">Pareja obvia</string>
    <string name="hint_strategy_naked_triple">Trío obvio</string>
    <string name="show_mistake_counter_summary">Muestra el número de entradas incorrectas realizadas durante la resolución del puzle.</string>
    <string name="show_mistake_counter">Mostrar el contador de errores</string>
    <string name="hint_strategy_hidden_pair">Pareja oculta</string>
    <string name="hint_strategy_hidden_triple">Trío oculto</string>
    <string name="hint_strategy_hidden_quad">Cuarteto oculto</string>
    <string name="hint_strategy_pointing_pair">Pareja apuntadora</string>
    <string name="hint_strategy_pointing_triple">Trío apuntador</string>
    <string name="hint_strategy_claiming_pair">Pareja demandante</string>
    <string name="hint_strategy_claiming_triple">Trío demandante</string>
    <string name="hint_strategy_xy_wing">XY-Wing</string>
    <string name="hint_strategy_xyz_wing">XYZ-Wing</string>
    <string name="hint_strategy_wxyz_wing">WXYZ-Wing</string>
    <string name="hint_strategy_fish_basic_2n">X-Wing</string>
    <string name="hint_strategy_fish_basic_3n">Pez espada</string>
    <string name="hint_strategy_fish_basic_6n">Ballena</string>
    <string name="hint_strategy_fish_basic_7n">Leviatán</string>
    <string name="hint_strategy_turbot_skyscraper_roofs">⊞ Techos ➠ %1$s</string>
    <string name="hint_strategy_fish_basic_4n">Medusa</string>
    <string name="hint_strategy_fish_basic_5n">Squirmbag o estrella de mar</string>
    <string name="hint_strategy_fish_basic_cover_set">⊞ Conjunto cubierta ➠ %1$s</string>
    <string name="xmas_challenge">🎄🎅 Desafío navideño 🎅🎄</string>
    <string name="audio_service_is_not_available">El servicio de audio no está disponible</string>
    <string name="hint_strategy_turbot_skyscraper">Rascacielos</string>
    <string name="hint_strategy_turbot_skyscraper_base">⊞ Base ➠ %1$s ➠ %2$s</string>
    <string name="hint_strategy_turbot_skyscraper_walls">⊞ Paredes ➠ %1$s</string>
    <string name="hint_strategy_fish_basic_base_set">⊞ Conjunto base ➠ %1$s</string>
    <string name="challenge">Desafío</string>
    <string name="error_could_not_find_puzzle_in_database">Error: No se ha podido encontrar el puzle en la base de datos.</string>
    <string name="hint_strategy_turbot_2_string_kite">Cometa de dos cuerdas</string>
    <string name="hint_strategy_turbot_2_string_kite_row">⊞ Fila ➠ %1$s ➠ %2$s</string>
    <string name="hint_strategy_turbot_2_string_kite_box">⊞ Casilla ➠ %1$s ➠ %2$s</string>
    <string name="hint_strategy_turbot_2_string_kite_col">⊞ Columna ➠ %1$s ➠ %2$s</string>
    <string name="hint_strategy_turbot_crane">Rodaballo - Grúa</string>
    <string name="homepage_introduction">Encontrarás más información (incluido cómo comunicar ideas y problemas) en la página de inicio del proyecto:</string>
    <string name="app_features">Características:\n\t• Sin anuncios\n\t• Los puzles pueden descargarse de la web, introducirse a mano o generarse en la página de inicio del proyecto\n\t• Estadísticas del juego (tiempo empleado, errores)\n\t• Sugerencias para el siguiente paso con múltiples estrategias\n\t• Importación y exportación desde un archivo\n\t• Temas visuales, incluido un editor de temas personalizado\n\t• Varios modos de entrada (teclado numérico, emergente, teclado, etc.)\n\t• Marcas de lápiz dobles\n…y mucho más.</string>
    <string name="home_page_label">Página principal</string>
    <string name="short_app_description">Open Sudoku es un juego de Sudoku de código abierto.</string>
    <string name="hint_strategy_turbot_crane_box">⊞ Casilla ➠ %1$s ➠ %2$s</string>
    <string name="hint_strategy_turbot_crane_row">⊞ Fila ➠ %1$s ➠ %2$s</string>
    <string name="hint_strategy_turbot_crane_col">⊞ Columna ➠ %1$s ➠ %2$s</string>
    <string name="hint_strategy_empty_rectangle_pair">⊞ Par ➠ %1$s ➠ %2$s</string>
    <string name="full_change_log">La versión completa del registro de cambios de las versiones anteriores está disponible en el archivo CHANGELOG.md en línea:</string>
    <plurals name="puzzle_processed_status">
        <item quantity="one">Procesado %s puzle.</item>
        <item quantity="many">Procesados %s de puzles.</item>
        <item quantity="other">Procesados %s puzles.</item>
    </plurals>
    <string name="hint_strategy_empty_rectangle_box">⊞ Casilla ➠ %1$s ➠ %2$s ➠ %3$s</string>
    <string name="hint_strategy_empty_rectangle">Rectángulo vacío</string>
    <string name="hint_strategy_empty_rectangle_cell">⊞ Celda ➠ %1$s ➠ %2$s</string>
    <string name="changes_in_v4_3_7">\t• Nueva estrategia (Rectángulo vacío). [Rainer Popella]\n\t• Mejorada la capacidad de respuesta de la aplicación [Bart Uliasz]\n\t• Solucionados los problemas de rotación de pantalla en importar/exportar y la cancelación en el botón atrás [Bart Uliasz]\n\t• Corrección en la actualización de la lista de carpetas [Bart Uliasz]\n\t• Actualizaciones de las traducciones [Bart Uliasz]</string>
</resources>
