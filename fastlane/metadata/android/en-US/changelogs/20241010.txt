* Option to disable completed number blinking in Settings
* Added Settings option to disable mistake counter
* Bugfixes (cells highlighting)
* Translations updates